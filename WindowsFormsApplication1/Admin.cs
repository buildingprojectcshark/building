﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Admin : Resident // וועד בית
    {
        DB dataBase = DB.GetInstance();
        private string totalPayBills;
        private string calling; // (option for maintence call)
        private string addRemoveUser;
        private string maintainenceRoom;
        private int seniority;

        public Admin() { }
        public Admin(string FirstName, string LastName,
            int Id, int Telephone, string Email, string username, string password, string tag, string totalPayBills, string calling,
            string addRemoveUser, string maintainenceRoom, int seniority, string bildingAdress) :
            base(FirstName, LastName, Id, Telephone, Email, username, password, tag = "Admin", bildingAdress)
        {
            this.totalPayBills = totalPayBills;
            this.calling = calling;
            this.addRemoveUser = addRemoveUser;
            this.maintainenceRoom = maintainenceRoom;
            this.seniority = seniority;
        }
        public string TotalPayBills
        {
            get { return this.totalPayBills; }
            set { this.totalPayBills = value; }
        }
        public string Calling
        {
            get { return this.calling; }
            set { this.calling = value; }
        }
        public string AddRemoveUser
        {
            get { return this.addRemoveUser; }
            set { this.addRemoveUser = value; }
        }
        public string MaintainenceRoom
        {
            get { return this.maintainenceRoom; }
            set { this.maintainenceRoom = value; }
        }
        public int Seniority
        {
            get { return this.seniority; }
            set { this.seniority = value; }
        }
        public List<User> remainingBills()
        { 
           return dataBase.RemainingBills();
           
        }

       /* public void payedBills()
        {
            dataBase.ViewPayment();
        }*/

        public int AddUser(User user)
        {
            int ok;                    
          ok=dataBase.AdddUser(user);
          return ok;

        }

        public int removeUser(int id)
        {
            int ok;
            ok=dataBase.RemoveUser(id);
            return ok;

        }

        public void alertAll(string alert)
        {
            int to = 0;
            string a = alert;
            Message aler = new Message(a, this.Id, to);

        }

        public override string ToString()
        {

            return base.ToString() + "    \t\nIs Admin\n   \t\n****************************************";
        }

        public override void sendMessage(string text, int to)
        {
            Message mess = new Message(text, this.Id, to);
            dataBase.SendMessage(mess);
        }


        public List<Order> printOrder()
        {
         return  dataBase.PrintOrders();

        }
       
        public List<Resident> printUsers(int userid)
        {
            return dataBase.PrintUsers(userid);
        }


        public List<Message> ViewReport()//see all reports from db
        {
            return dataBase.ViewReport();

        }

        public List<Message> ViewRequest()//see all request from db
        {

            return dataBase.ViewRequest();
        }
        public List<Assignments> ViewAssignments()//see all Assignments from db
        {
          return dataBase.ViewAssignments();

        }
        public List<Message> ViewMessage(int fromId)
        {
            return dataBase.ViewMessage(fromId);

        }
        public List<Payment> ViewPayment()
    {
        return dataBase.ViewPayment();
    }


   
    }
}


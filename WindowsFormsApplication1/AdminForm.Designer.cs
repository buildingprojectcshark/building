﻿namespace WindowsFormsApplication1
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminForm));
            this.welcome = new System.Windows.Forms.Label();
            this.btnSendMessage = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnViewPayments = new System.Windows.Forms.Button();
            this.btnUserManagement = new System.Windows.Forms.Button();
            this.btnAddRemoveUser_Admin = new System.Windows.Forms.Button();
            this.btnRecieveMessage = new System.Windows.Forms.Button();
            this.btnSendMessage_Admin = new System.Windows.Forms.Button();
            this.txtMessage_Admin = new System.Windows.Forms.TextBox();
            this.txtTo_Admin = new System.Windows.Forms.TextBox();
            this.lblSendTo_Admin = new System.Windows.Forms.Label();
            this.txtViewMessage_Admin = new System.Windows.Forms.TextBox();
            this.btnNextMessage_Admin = new System.Windows.Forms.Button();
            this.btnAllPayments_Admin = new System.Windows.Forms.Button();
            this.btnNotPayed_Admin = new System.Windows.Forms.Button();
            this.btnRecievedPayment_admin = new System.Windows.Forms.Button();
            this.txtViewPayments_Admin = new System.Windows.Forms.TextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.rBtnShowAllUsers_Admin = new System.Windows.Forms.RadioButton();
            this.rBtnShowUserByID_Admin = new System.Windows.Forms.RadioButton();
            this.btnShow_UManagementAdmin = new System.Windows.Forms.Button();
            this.txtShowUsers_Admin = new System.Windows.Forms.TextBox();
            this.txtID_UserManagementAdmin = new System.Windows.Forms.TextBox();
            this.lblID_UserManagementAdmin = new System.Windows.Forms.Label();
            this.txtFirstName_AddUser = new System.Windows.Forms.TextBox();
            this.txtLastName_AddUser = new System.Windows.Forms.TextBox();
            this.txtEmail_AddUser = new System.Windows.Forms.TextBox();
            this.txtID_AddUser = new System.Windows.Forms.TextBox();
            this.txtIDToDelete_RemoveUser = new System.Windows.Forms.TextBox();
            this.btnADDUser_Admin = new System.Windows.Forms.Button();
            this.btnRemoveUser_Admin = new System.Windows.Forms.Button();
            this.btnViewAssignments_Admin = new System.Windows.Forms.Button();
            this.btnNext_ViewAssignments = new System.Windows.Forms.Button();
            this.txtViewAssignments_ViewAssignments = new System.Windows.Forms.TextBox();
            this.btnViewRequest_Admin = new System.Windows.Forms.Button();
            this.btnNext_ViewRequest = new System.Windows.Forms.Button();
            this.txtViewRequest_Admin = new System.Windows.Forms.TextBox();
            this.btnViewReport_Admin = new System.Windows.Forms.Button();
            this.btnNetx_ViewReport = new System.Windows.Forms.Button();
            this.txtViewReport_Admin = new System.Windows.Forms.TextBox();
            this.btnViewOrders_Admin = new System.Windows.Forms.Button();
            this.txtVieOrders_Admin = new System.Windows.Forms.TextBox();
            this.btnViewAll_Orders_Admin = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // welcome
            // 
            this.welcome.AutoSize = true;
            this.welcome.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.welcome.Location = new System.Drawing.Point(8, 24);
            this.welcome.Name = "welcome";
            this.welcome.Size = new System.Drawing.Size(120, 22);
            this.welcome.TabIndex = 0;
            this.welcome.Text = "Hello Admin";
            // 
            // btnSendMessage
            // 
            this.btnSendMessage.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSendMessage.Location = new System.Drawing.Point(12, 49);
            this.btnSendMessage.Name = "btnSendMessage";
            this.btnSendMessage.Size = new System.Drawing.Size(138, 23);
            this.btnSendMessage.TabIndex = 5;
            this.btnSendMessage.Text = "Send Message";
            this.btnSendMessage.UseVisualStyleBackColor = true;
            this.btnSendMessage.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // btnBack
            // 
            this.btnBack.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Location = new System.Drawing.Point(12, 322);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 6;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.button6_Click);
            // 
            // btnViewPayments
            // 
            this.btnViewPayments.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnViewPayments.Location = new System.Drawing.Point(12, 107);
            this.btnViewPayments.Name = "btnViewPayments";
            this.btnViewPayments.Size = new System.Drawing.Size(138, 23);
            this.btnViewPayments.TabIndex = 8;
            this.btnViewPayments.Text = "View Payments";
            this.btnViewPayments.UseVisualStyleBackColor = true;
            this.btnViewPayments.Click += new System.EventHandler(this.btnViewPayments_Click);
            // 
            // btnUserManagement
            // 
            this.btnUserManagement.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnUserManagement.Location = new System.Drawing.Point(12, 136);
            this.btnUserManagement.Name = "btnUserManagement";
            this.btnUserManagement.Size = new System.Drawing.Size(138, 23);
            this.btnUserManagement.TabIndex = 9;
            this.btnUserManagement.Text = "User Management";
            this.btnUserManagement.UseVisualStyleBackColor = true;
            this.btnUserManagement.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnAddRemoveUser_Admin
            // 
            this.btnAddRemoveUser_Admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnAddRemoveUser_Admin.Location = new System.Drawing.Point(12, 165);
            this.btnAddRemoveUser_Admin.Name = "btnAddRemoveUser_Admin";
            this.btnAddRemoveUser_Admin.Size = new System.Drawing.Size(138, 23);
            this.btnAddRemoveUser_Admin.TabIndex = 10;
            this.btnAddRemoveUser_Admin.Text = "Add/Remove User";
            this.btnAddRemoveUser_Admin.UseVisualStyleBackColor = true;
            this.btnAddRemoveUser_Admin.Click += new System.EventHandler(this.btnAddRemoveUser_Admin_Click);
            // 
            // btnRecieveMessage
            // 
            this.btnRecieveMessage.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnRecieveMessage.Location = new System.Drawing.Point(13, 78);
            this.btnRecieveMessage.Name = "btnRecieveMessage";
            this.btnRecieveMessage.Size = new System.Drawing.Size(137, 23);
            this.btnRecieveMessage.TabIndex = 11;
            this.btnRecieveMessage.Text = "Recieve Message";
            this.btnRecieveMessage.UseVisualStyleBackColor = true;
            this.btnRecieveMessage.Click += new System.EventHandler(this.btnRecieveMessage_Click);
            // 
            // btnSendMessage_Admin
            // 
            this.btnSendMessage_Admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnSendMessage_Admin.Location = new System.Drawing.Point(545, 163);
            this.btnSendMessage_Admin.Name = "btnSendMessage_Admin";
            this.btnSendMessage_Admin.Size = new System.Drawing.Size(75, 23);
            this.btnSendMessage_Admin.TabIndex = 12;
            this.btnSendMessage_Admin.Text = "Send";
            this.btnSendMessage_Admin.UseVisualStyleBackColor = true;
            this.btnSendMessage_Admin.Visible = false;
            this.btnSendMessage_Admin.Click += new System.EventHandler(this.btnSendMessage_Admin_Click);
            // 
            // txtMessage_Admin
            // 
            this.txtMessage_Admin.Location = new System.Drawing.Point(157, 107);
            this.txtMessage_Admin.Multiline = true;
            this.txtMessage_Admin.Name = "txtMessage_Admin";
            this.txtMessage_Admin.Size = new System.Drawing.Size(380, 81);
            this.txtMessage_Admin.TabIndex = 13;
            this.txtMessage_Admin.Visible = false;
            // 
            // txtTo_Admin
            // 
            this.txtTo_Admin.Location = new System.Drawing.Point(436, 51);
            this.txtTo_Admin.Name = "txtTo_Admin";
            this.txtTo_Admin.Size = new System.Drawing.Size(100, 20);
            this.txtTo_Admin.TabIndex = 14;
            this.txtTo_Admin.Visible = false;
            // 
            // lblSendTo_Admin
            // 
            this.lblSendTo_Admin.AutoSize = true;
            this.lblSendTo_Admin.BackColor = System.Drawing.Color.Transparent;
            this.lblSendTo_Admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.lblSendTo_Admin.Location = new System.Drawing.Point(339, 56);
            this.lblSendTo_Admin.Name = "lblSendTo_Admin";
            this.lblSendTo_Admin.Size = new System.Drawing.Size(63, 15);
            this.lblSendTo_Admin.TabIndex = 15;
            this.lblSendTo_Admin.Text = "Send To:";
            this.lblSendTo_Admin.Visible = false;
            // 
            // txtViewMessage_Admin
            // 
            this.txtViewMessage_Admin.Location = new System.Drawing.Point(158, 49);
            this.txtViewMessage_Admin.Multiline = true;
            this.txtViewMessage_Admin.Name = "txtViewMessage_Admin";
            this.txtViewMessage_Admin.ReadOnly = true;
            this.txtViewMessage_Admin.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtViewMessage_Admin.Size = new System.Drawing.Size(378, 139);
            this.txtViewMessage_Admin.TabIndex = 16;
            this.txtViewMessage_Admin.Visible = false;
            // 
            // btnNextMessage_Admin
            // 
            this.btnNextMessage_Admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnNextMessage_Admin.Location = new System.Drawing.Point(545, 165);
            this.btnNextMessage_Admin.Name = "btnNextMessage_Admin";
            this.btnNextMessage_Admin.Size = new System.Drawing.Size(75, 23);
            this.btnNextMessage_Admin.TabIndex = 17;
            this.btnNextMessage_Admin.Text = "Next";
            this.btnNextMessage_Admin.UseVisualStyleBackColor = true;
            this.btnNextMessage_Admin.Visible = false;
            this.btnNextMessage_Admin.Click += new System.EventHandler(this.btnNextMessage_Admin_Click);
            // 
            // btnAllPayments_Admin
            // 
            this.btnAllPayments_Admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnAllPayments_Admin.Location = new System.Drawing.Point(526, 47);
            this.btnAllPayments_Admin.Name = "btnAllPayments_Admin";
            this.btnAllPayments_Admin.Size = new System.Drawing.Size(94, 23);
            this.btnAllPayments_Admin.TabIndex = 18;
            this.btnAllPayments_Admin.Text = "All";
            this.btnAllPayments_Admin.UseVisualStyleBackColor = true;
            this.btnAllPayments_Admin.Visible = false;
            this.btnAllPayments_Admin.Click += new System.EventHandler(this.btnAllPayments_Admin_Click);
            // 
            // btnNotPayed_Admin
            // 
            this.btnNotPayed_Admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnNotPayed_Admin.Location = new System.Drawing.Point(521, 82);
            this.btnNotPayed_Admin.Name = "btnNotPayed_Admin";
            this.btnNotPayed_Admin.Size = new System.Drawing.Size(93, 23);
            this.btnNotPayed_Admin.TabIndex = 19;
            this.btnNotPayed_Admin.Text = "Remaining";
            this.btnNotPayed_Admin.UseVisualStyleBackColor = true;
            this.btnNotPayed_Admin.Visible = false;
            this.btnNotPayed_Admin.Click += new System.EventHandler(this.btnNotPayed_Admin_Click);
            // 
            // btnRecievedPayment_admin
            // 
            this.btnRecievedPayment_admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnRecievedPayment_admin.Location = new System.Drawing.Point(528, 125);
            this.btnRecievedPayment_admin.Name = "btnRecievedPayment_admin";
            this.btnRecievedPayment_admin.Size = new System.Drawing.Size(94, 23);
            this.btnRecievedPayment_admin.TabIndex = 20;
            this.btnRecievedPayment_admin.Text = "Recieved";
            this.btnRecievedPayment_admin.UseVisualStyleBackColor = true;
            this.btnRecievedPayment_admin.Visible = false;
            this.btnRecievedPayment_admin.Click += new System.EventHandler(this.btnRecievedPayment_admin_Click);
            // 
            // txtViewPayments_Admin
            // 
            this.txtViewPayments_Admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.txtViewPayments_Admin.Location = new System.Drawing.Point(155, 84);
            this.txtViewPayments_Admin.Multiline = true;
            this.txtViewPayments_Admin.Name = "txtViewPayments_Admin";
            this.txtViewPayments_Admin.ReadOnly = true;
            this.txtViewPayments_Admin.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtViewPayments_Admin.Size = new System.Drawing.Size(361, 139);
            this.txtViewPayments_Admin.TabIndex = 21;
            this.txtViewPayments_Admin.Visible = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // rBtnShowAllUsers_Admin
            // 
            this.rBtnShowAllUsers_Admin.AutoSize = true;
            this.rBtnShowAllUsers_Admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.rBtnShowAllUsers_Admin.Location = new System.Drawing.Point(479, 52);
            this.rBtnShowAllUsers_Admin.Name = "rBtnShowAllUsers_Admin";
            this.rBtnShowAllUsers_Admin.Size = new System.Drawing.Size(122, 19);
            this.rBtnShowAllUsers_Admin.TabIndex = 22;
            this.rBtnShowAllUsers_Admin.TabStop = true;
            this.rBtnShowAllUsers_Admin.Text = "Show All Users";
            this.rBtnShowAllUsers_Admin.UseVisualStyleBackColor = true;
            this.rBtnShowAllUsers_Admin.Visible = false;
            // 
            // rBtnShowUserByID_Admin
            // 
            this.rBtnShowUserByID_Admin.AutoSize = true;
            this.rBtnShowUserByID_Admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.rBtnShowUserByID_Admin.Location = new System.Drawing.Point(479, 84);
            this.rBtnShowUserByID_Admin.Name = "rBtnShowUserByID_Admin";
            this.rBtnShowUserByID_Admin.Size = new System.Drawing.Size(131, 19);
            this.rBtnShowUserByID_Admin.TabIndex = 23;
            this.rBtnShowUserByID_Admin.TabStop = true;
            this.rBtnShowUserByID_Admin.Text = "Show User By ID";
            this.rBtnShowUserByID_Admin.UseVisualStyleBackColor = true;
            this.rBtnShowUserByID_Admin.Visible = false;
            this.rBtnShowUserByID_Admin.CheckedChanged += new System.EventHandler(this.rBtnShowUserByID_Admin_CheckedChanged);
            // 
            // btnShow_UManagementAdmin
            // 
            this.btnShow_UManagementAdmin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnShow_UManagementAdmin.Location = new System.Drawing.Point(528, 236);
            this.btnShow_UManagementAdmin.Name = "btnShow_UManagementAdmin";
            this.btnShow_UManagementAdmin.Size = new System.Drawing.Size(58, 23);
            this.btnShow_UManagementAdmin.TabIndex = 24;
            this.btnShow_UManagementAdmin.Text = "Show";
            this.btnShow_UManagementAdmin.UseVisualStyleBackColor = true;
            this.btnShow_UManagementAdmin.Visible = false;
            this.btnShow_UManagementAdmin.Click += new System.EventHandler(this.btnShow_UManagementAdmin_Click);
            // 
            // txtShowUsers_Admin
            // 
            this.txtShowUsers_Admin.Location = new System.Drawing.Point(168, 47);
            this.txtShowUsers_Admin.Multiline = true;
            this.txtShowUsers_Admin.Name = "txtShowUsers_Admin";
            this.txtShowUsers_Admin.ReadOnly = true;
            this.txtShowUsers_Admin.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtShowUsers_Admin.Size = new System.Drawing.Size(291, 230);
            this.txtShowUsers_Admin.TabIndex = 25;
            this.txtShowUsers_Admin.Visible = false;
            // 
            // txtID_UserManagementAdmin
            // 
            this.txtID_UserManagementAdmin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.txtID_UserManagementAdmin.Location = new System.Drawing.Point(488, 167);
            this.txtID_UserManagementAdmin.Name = "txtID_UserManagementAdmin";
            this.txtID_UserManagementAdmin.Size = new System.Drawing.Size(131, 23);
            this.txtID_UserManagementAdmin.TabIndex = 26;
            this.txtID_UserManagementAdmin.Visible = false;
            // 
            // lblID_UserManagementAdmin
            // 
            this.lblID_UserManagementAdmin.AutoSize = true;
            this.lblID_UserManagementAdmin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.lblID_UserManagementAdmin.Location = new System.Drawing.Point(479, 145);
            this.lblID_UserManagementAdmin.Name = "lblID_UserManagementAdmin";
            this.lblID_UserManagementAdmin.Size = new System.Drawing.Size(64, 15);
            this.lblID_UserManagementAdmin.TabIndex = 27;
            this.lblID_UserManagementAdmin.Text = "Enter ID:";
            this.lblID_UserManagementAdmin.Visible = false;
            // 
            // txtFirstName_AddUser
            // 
            this.txtFirstName_AddUser.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.txtFirstName_AddUser.Location = new System.Drawing.Point(158, 49);
            this.txtFirstName_AddUser.Name = "txtFirstName_AddUser";
            this.txtFirstName_AddUser.Size = new System.Drawing.Size(290, 23);
            this.txtFirstName_AddUser.TabIndex = 28;
            this.txtFirstName_AddUser.Text = "Enter First Name";
            this.txtFirstName_AddUser.Visible = false;
            // 
            // txtLastName_AddUser
            // 
            this.txtLastName_AddUser.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.txtLastName_AddUser.Location = new System.Drawing.Point(158, 84);
            this.txtLastName_AddUser.Name = "txtLastName_AddUser";
            this.txtLastName_AddUser.Size = new System.Drawing.Size(290, 23);
            this.txtLastName_AddUser.TabIndex = 29;
            this.txtLastName_AddUser.Text = "Enter Last Name";
            this.txtLastName_AddUser.Visible = false;
            // 
            // txtEmail_AddUser
            // 
            this.txtEmail_AddUser.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.txtEmail_AddUser.Location = new System.Drawing.Point(158, 127);
            this.txtEmail_AddUser.Name = "txtEmail_AddUser";
            this.txtEmail_AddUser.Size = new System.Drawing.Size(290, 23);
            this.txtEmail_AddUser.TabIndex = 30;
            this.txtEmail_AddUser.Text = "Enter Email";
            this.txtEmail_AddUser.Visible = false;
            // 
            // txtID_AddUser
            // 
            this.txtID_AddUser.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.txtID_AddUser.Location = new System.Drawing.Point(157, 165);
            this.txtID_AddUser.Name = "txtID_AddUser";
            this.txtID_AddUser.Size = new System.Drawing.Size(290, 23);
            this.txtID_AddUser.TabIndex = 31;
            this.txtID_AddUser.Text = "Enter ID";
            this.txtID_AddUser.Visible = false;
            // 
            // txtIDToDelete_RemoveUser
            // 
            this.txtIDToDelete_RemoveUser.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.txtIDToDelete_RemoveUser.Location = new System.Drawing.Point(157, 235);
            this.txtIDToDelete_RemoveUser.Name = "txtIDToDelete_RemoveUser";
            this.txtIDToDelete_RemoveUser.Size = new System.Drawing.Size(291, 23);
            this.txtIDToDelete_RemoveUser.TabIndex = 32;
            this.txtIDToDelete_RemoveUser.Text = "Enter ID of User You Want To Delete";
            this.txtIDToDelete_RemoveUser.Visible = false;
            // 
            // btnADDUser_Admin
            // 
            this.btnADDUser_Admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnADDUser_Admin.Location = new System.Drawing.Point(503, 84);
            this.btnADDUser_Admin.Name = "btnADDUser_Admin";
            this.btnADDUser_Admin.Size = new System.Drawing.Size(75, 48);
            this.btnADDUser_Admin.TabIndex = 33;
            this.btnADDUser_Admin.Text = "ADD";
            this.btnADDUser_Admin.UseVisualStyleBackColor = true;
            this.btnADDUser_Admin.Visible = false;
            this.btnADDUser_Admin.Click += new System.EventHandler(this.btnADDUser_Admin_Click);
            // 
            // btnRemoveUser_Admin
            // 
            this.btnRemoveUser_Admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnRemoveUser_Admin.Location = new System.Drawing.Point(512, 236);
            this.btnRemoveUser_Admin.Name = "btnRemoveUser_Admin";
            this.btnRemoveUser_Admin.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveUser_Admin.TabIndex = 34;
            this.btnRemoveUser_Admin.Text = "Remove";
            this.btnRemoveUser_Admin.UseVisualStyleBackColor = true;
            this.btnRemoveUser_Admin.Visible = false;
            this.btnRemoveUser_Admin.Click += new System.EventHandler(this.btnRemoveUser_Admin_Click);
            // 
            // btnViewAssignments_Admin
            // 
            this.btnViewAssignments_Admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnViewAssignments_Admin.Location = new System.Drawing.Point(13, 194);
            this.btnViewAssignments_Admin.Margin = new System.Windows.Forms.Padding(2);
            this.btnViewAssignments_Admin.Name = "btnViewAssignments_Admin";
            this.btnViewAssignments_Admin.Size = new System.Drawing.Size(137, 21);
            this.btnViewAssignments_Admin.TabIndex = 35;
            this.btnViewAssignments_Admin.Text = "Veiw Assignments";
            this.btnViewAssignments_Admin.UseVisualStyleBackColor = true;
            this.btnViewAssignments_Admin.Click += new System.EventHandler(this.btnViewAssignments_Admin_Click);
            // 
            // btnNext_ViewAssignments
            // 
            this.btnNext_ViewAssignments.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnNext_ViewAssignments.Location = new System.Drawing.Point(512, 236);
            this.btnNext_ViewAssignments.Margin = new System.Windows.Forms.Padding(2);
            this.btnNext_ViewAssignments.Name = "btnNext_ViewAssignments";
            this.btnNext_ViewAssignments.Size = new System.Drawing.Size(75, 24);
            this.btnNext_ViewAssignments.TabIndex = 36;
            this.btnNext_ViewAssignments.Text = "Next";
            this.btnNext_ViewAssignments.UseVisualStyleBackColor = true;
            this.btnNext_ViewAssignments.Visible = false;
            this.btnNext_ViewAssignments.Click += new System.EventHandler(this.btnNext_ViewAssignments_Click);
            // 
            // txtViewAssignments_ViewAssignments
            // 
            this.txtViewAssignments_ViewAssignments.Location = new System.Drawing.Point(168, 47);
            this.txtViewAssignments_ViewAssignments.Margin = new System.Windows.Forms.Padding(2);
            this.txtViewAssignments_ViewAssignments.Multiline = true;
            this.txtViewAssignments_ViewAssignments.Name = "txtViewAssignments_ViewAssignments";
            this.txtViewAssignments_ViewAssignments.ReadOnly = true;
            this.txtViewAssignments_ViewAssignments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtViewAssignments_ViewAssignments.Size = new System.Drawing.Size(291, 233);
            this.txtViewAssignments_ViewAssignments.TabIndex = 37;
            this.txtViewAssignments_ViewAssignments.Visible = false;
            // 
            // btnViewRequest_Admin
            // 
            this.btnViewRequest_Admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnViewRequest_Admin.Location = new System.Drawing.Point(13, 220);
            this.btnViewRequest_Admin.Margin = new System.Windows.Forms.Padding(2);
            this.btnViewRequest_Admin.Name = "btnViewRequest_Admin";
            this.btnViewRequest_Admin.Size = new System.Drawing.Size(137, 23);
            this.btnViewRequest_Admin.TabIndex = 38;
            this.btnViewRequest_Admin.Text = "View Request";
            this.btnViewRequest_Admin.UseVisualStyleBackColor = true;
            this.btnViewRequest_Admin.Click += new System.EventHandler(this.btnViewRequest_Admin_Click);
            // 
            // btnNext_ViewRequest
            // 
            this.btnNext_ViewRequest.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnNext_ViewRequest.Location = new System.Drawing.Point(512, 236);
            this.btnNext_ViewRequest.Margin = new System.Windows.Forms.Padding(2);
            this.btnNext_ViewRequest.Name = "btnNext_ViewRequest";
            this.btnNext_ViewRequest.Size = new System.Drawing.Size(75, 24);
            this.btnNext_ViewRequest.TabIndex = 39;
            this.btnNext_ViewRequest.Text = "Next";
            this.btnNext_ViewRequest.UseVisualStyleBackColor = true;
            this.btnNext_ViewRequest.Visible = false;
            this.btnNext_ViewRequest.Click += new System.EventHandler(this.btnNext_ViewRequest_Click);
            // 
            // txtViewRequest_Admin
            // 
            this.txtViewRequest_Admin.Location = new System.Drawing.Point(167, 49);
            this.txtViewRequest_Admin.Margin = new System.Windows.Forms.Padding(2);
            this.txtViewRequest_Admin.Multiline = true;
            this.txtViewRequest_Admin.Name = "txtViewRequest_Admin";
            this.txtViewRequest_Admin.ReadOnly = true;
            this.txtViewRequest_Admin.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtViewRequest_Admin.Size = new System.Drawing.Size(292, 233);
            this.txtViewRequest_Admin.TabIndex = 40;
            this.txtViewRequest_Admin.Visible = false;
            // 
            // btnViewReport_Admin
            // 
            this.btnViewReport_Admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnViewReport_Admin.Location = new System.Drawing.Point(13, 249);
            this.btnViewReport_Admin.Margin = new System.Windows.Forms.Padding(2);
            this.btnViewReport_Admin.Name = "btnViewReport_Admin";
            this.btnViewReport_Admin.Size = new System.Drawing.Size(137, 21);
            this.btnViewReport_Admin.TabIndex = 41;
            this.btnViewReport_Admin.Text = "View Report";
            this.btnViewReport_Admin.UseVisualStyleBackColor = true;
            this.btnViewReport_Admin.Click += new System.EventHandler(this.btnViewReport_Admin_Click);
            // 
            // btnNetx_ViewReport
            // 
            this.btnNetx_ViewReport.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnNetx_ViewReport.Location = new System.Drawing.Point(512, 235);
            this.btnNetx_ViewReport.Margin = new System.Windows.Forms.Padding(2);
            this.btnNetx_ViewReport.Name = "btnNetx_ViewReport";
            this.btnNetx_ViewReport.Size = new System.Drawing.Size(75, 24);
            this.btnNetx_ViewReport.TabIndex = 42;
            this.btnNetx_ViewReport.Text = "Next";
            this.btnNetx_ViewReport.UseVisualStyleBackColor = true;
            this.btnNetx_ViewReport.Visible = false;
            this.btnNetx_ViewReport.Click += new System.EventHandler(this.btnNetx_ViewReport_Click);
            // 
            // txtViewReport_Admin
            // 
            this.txtViewReport_Admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.txtViewReport_Admin.ForeColor = System.Drawing.Color.Black;
            this.txtViewReport_Admin.Location = new System.Drawing.Point(176, 51);
            this.txtViewReport_Admin.Margin = new System.Windows.Forms.Padding(2);
            this.txtViewReport_Admin.Multiline = true;
            this.txtViewReport_Admin.Name = "txtViewReport_Admin";
            this.txtViewReport_Admin.ReadOnly = true;
            this.txtViewReport_Admin.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtViewReport_Admin.Size = new System.Drawing.Size(292, 233);
            this.txtViewReport_Admin.TabIndex = 43;
            this.txtViewReport_Admin.Visible = false;
            // 
            // btnViewOrders_Admin
            // 
            this.btnViewOrders_Admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnViewOrders_Admin.Location = new System.Drawing.Point(12, 276);
            this.btnViewOrders_Admin.Name = "btnViewOrders_Admin";
            this.btnViewOrders_Admin.Size = new System.Drawing.Size(138, 23);
            this.btnViewOrders_Admin.TabIndex = 44;
            this.btnViewOrders_Admin.Text = "View Orders ";
            this.btnViewOrders_Admin.UseVisualStyleBackColor = true;
            this.btnViewOrders_Admin.Click += new System.EventHandler(this.btnViewOrders_Admin_Click);
            // 
            // txtVieOrders_Admin
            // 
            this.txtVieOrders_Admin.Location = new System.Drawing.Point(157, 49);
            this.txtVieOrders_Admin.Multiline = true;
            this.txtVieOrders_Admin.Name = "txtVieOrders_Admin";
            this.txtVieOrders_Admin.ReadOnly = true;
            this.txtVieOrders_Admin.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtVieOrders_Admin.Size = new System.Drawing.Size(311, 235);
            this.txtVieOrders_Admin.TabIndex = 45;
            this.txtVieOrders_Admin.Visible = false;
            // 
            // btnViewAll_Orders_Admin
            // 
            this.btnViewAll_Orders_Admin.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnViewAll_Orders_Admin.Location = new System.Drawing.Point(512, 235);
            this.btnViewAll_Orders_Admin.Name = "btnViewAll_Orders_Admin";
            this.btnViewAll_Orders_Admin.Size = new System.Drawing.Size(75, 23);
            this.btnViewAll_Orders_Admin.TabIndex = 46;
            this.btnViewAll_Orders_Admin.Text = "View All";
            this.btnViewAll_Orders_Admin.UseVisualStyleBackColor = true;
            this.btnViewAll_Orders_Admin.Visible = false;
            this.btnViewAll_Orders_Admin.Click += new System.EventHandler(this.btnViewAll_Orders_Admin_Click);
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(649, 357);
            this.Controls.Add(this.btnViewAll_Orders_Admin);
            this.Controls.Add(this.txtVieOrders_Admin);
            this.Controls.Add(this.btnViewOrders_Admin);
            this.Controls.Add(this.txtViewReport_Admin);
            this.Controls.Add(this.btnNetx_ViewReport);
            this.Controls.Add(this.btnViewReport_Admin);
            this.Controls.Add(this.txtViewRequest_Admin);
            this.Controls.Add(this.btnNext_ViewRequest);
            this.Controls.Add(this.btnViewRequest_Admin);
            this.Controls.Add(this.txtViewAssignments_ViewAssignments);
            this.Controls.Add(this.btnNext_ViewAssignments);
            this.Controls.Add(this.btnViewAssignments_Admin);
            this.Controls.Add(this.btnRemoveUser_Admin);
            this.Controls.Add(this.btnADDUser_Admin);
            this.Controls.Add(this.txtIDToDelete_RemoveUser);
            this.Controls.Add(this.txtID_AddUser);
            this.Controls.Add(this.txtEmail_AddUser);
            this.Controls.Add(this.txtLastName_AddUser);
            this.Controls.Add(this.txtFirstName_AddUser);
            this.Controls.Add(this.lblID_UserManagementAdmin);
            this.Controls.Add(this.txtID_UserManagementAdmin);
            this.Controls.Add(this.txtShowUsers_Admin);
            this.Controls.Add(this.btnShow_UManagementAdmin);
            this.Controls.Add(this.rBtnShowUserByID_Admin);
            this.Controls.Add(this.rBtnShowAllUsers_Admin);
            this.Controls.Add(this.txtViewPayments_Admin);
            this.Controls.Add(this.btnRecievedPayment_admin);
            this.Controls.Add(this.btnNotPayed_Admin);
            this.Controls.Add(this.btnAllPayments_Admin);
            this.Controls.Add(this.btnNextMessage_Admin);
            this.Controls.Add(this.txtViewMessage_Admin);
            this.Controls.Add(this.lblSendTo_Admin);
            this.Controls.Add(this.txtTo_Admin);
            this.Controls.Add(this.txtMessage_Admin);
            this.Controls.Add(this.btnSendMessage_Admin);
            this.Controls.Add(this.btnRecieveMessage);
            this.Controls.Add(this.btnAddRemoveUser_Admin);
            this.Controls.Add(this.btnUserManagement);
            this.Controls.Add(this.btnViewPayments);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnSendMessage);
            this.Controls.Add(this.welcome);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AdminForm";
            this.Text = "Admin";
            this.Load += new System.EventHandler(this.AdminForm_Load);
            this.MdiChildActivate += new System.EventHandler(this.AdminForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void rBtnShowUserByID_Admin_CheckedChanged(object sender, System.EventArgs e)
        {
            
        }


       

        

        private void button1_Click(object sender, System.EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void btnAlertAll_Click(object sender, System.EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        

        private void btnTotalPayBills_Click(object sender, System.EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Label welcome;
        private System.Windows.Forms.Button btnSendMessage;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnViewPayments;
        private System.Windows.Forms.Button btnUserManagement;
        private System.Windows.Forms.Button btnAddRemoveUser_Admin;
        private System.Windows.Forms.Button btnRecieveMessage;
        private System.Windows.Forms.Button btnSendMessage_Admin;
        private System.Windows.Forms.TextBox txtMessage_Admin;
        private System.Windows.Forms.TextBox txtTo_Admin;
        private System.Windows.Forms.Label lblSendTo_Admin;
        private System.Windows.Forms.TextBox txtViewMessage_Admin;
        private System.Windows.Forms.Button btnNextMessage_Admin;
        private System.Windows.Forms.Button btnAllPayments_Admin;
        private System.Windows.Forms.Button btnNotPayed_Admin;
        private System.Windows.Forms.Button btnRecievedPayment_admin;
        private System.Windows.Forms.TextBox txtViewPayments_Admin;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.TextBox txtShowUsers_Admin;
        private System.Windows.Forms.Button btnShow_UManagementAdmin;
        private System.Windows.Forms.RadioButton rBtnShowUserByID_Admin;
        private System.Windows.Forms.RadioButton rBtnShowAllUsers_Admin;
        private System.Windows.Forms.Label lblID_UserManagementAdmin;
        private System.Windows.Forms.TextBox txtID_UserManagementAdmin;
        private System.Windows.Forms.Button btnRemoveUser_Admin;
        private System.Windows.Forms.Button btnADDUser_Admin;
        private System.Windows.Forms.TextBox txtIDToDelete_RemoveUser;
        private System.Windows.Forms.TextBox txtID_AddUser;
        private System.Windows.Forms.TextBox txtEmail_AddUser;
        private System.Windows.Forms.TextBox txtLastName_AddUser;
        private System.Windows.Forms.TextBox txtFirstName_AddUser;
        private System.Windows.Forms.Button btnViewAssignments_Admin;
        private System.Windows.Forms.Button btnNext_ViewAssignments;
        private System.Windows.Forms.TextBox txtViewAssignments_ViewAssignments;
        private System.Windows.Forms.Button btnViewRequest_Admin;
        private System.Windows.Forms.Button btnNext_ViewRequest;
        private System.Windows.Forms.TextBox txtViewRequest_Admin;
        private System.Windows.Forms.Button btnViewReport_Admin;
        private System.Windows.Forms.Button btnNetx_ViewReport;
        private System.Windows.Forms.TextBox txtViewReport_Admin;
        private System.Windows.Forms.Button btnViewAll_Orders_Admin;
        private System.Windows.Forms.TextBox txtVieOrders_Admin;
        private System.Windows.Forms.Button btnViewOrders_Admin;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    partial class 
        AdminForm : Form
    {
        Admin admin;
        static int index = 0; // message counter
        List<Message> reports;
        List<Message> requests;
        List<Message> messages;
        List<Assignments> assignments;
        List<Resident> users;
        List<Payment> payments;
        List<User> NotPayed;
        List<Order> orders;

        public AdminForm()
        {
            InitializeComponent();
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
        }
        public AdminForm(Admin admin)
        {
            this.admin = admin;
          
            InitializeComponent();
        }


            foreach (char c in text)
            {
                if (c < '0' || c > '9') ;
                MessageBox.Show(this, "you need to enter numbers only!", "Closing", MessageBoxButtons.YesNo);
                return false;
            }
            return true;
        }
         
   
        // Added closing on X click******************************************************************/
       
        
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;

            // Confirm user wants to close
            switch (MessageBox.Show(this, "Are you sure you want to close?", "Closing", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    e.Cancel = true;
  
                    break;
                default:

                    break;
            }
        }
        /************************************Back Button Click (Creates New Login Form)******************************************************/

        private void button6_Click(object sender, EventArgs e)
        {
            LoginForm back = new LoginForm();
            this.Hide();
            back.Show();
            this.Owner = back;
        }


        private void AdminForm_Load(object sender, EventArgs e)
        {
            welcome.Text = "Welcome " + admin.LastName.ToUpper() + " " + admin.FirstName.ToUpper();
        }
/*******************************messages buttons***********************************/
        
        private void btnSendMessage_Click(object sender, EventArgs e)
        {
            txtViewPayments_Admin.Visible = false;
            btnNotPayed_Admin.Visible = false;
            btnRecievedPayment_admin.Visible = false;
            btnAllPayments_Admin.Visible = false;
            txtViewMessage_Admin.Visible = false;
            btnNextMessage_Admin.Visible = false;
            txtShowUsers_Admin.Visible = false;
            rBtnShowAllUsers_Admin.Visible = false;
            rBtnShowUserByID_Admin.Visible = false;
            lblID_UserManagementAdmin.Visible = false;
            txtID_UserManagementAdmin.Visible = false;
            btnShow_UManagementAdmin.Visible = false;
            txtEmail_AddUser.Visible = false;
            txtFirstName_AddUser.Visible = false;
            txtLastName_AddUser.Visible = false;
            txtID_AddUser.Visible = false;
            txtIDToDelete_RemoveUser.Visible = false;
            btnADDUser_Admin.Visible = false;
            btnRemoveUser_Admin.Visible = false;
            txtViewAssignments_ViewAssignments.Visible = false;
            btnNext_ViewAssignments.Visible = false;
            txtViewRequest_Admin.Visible = false;
            btnNext_ViewRequest.Visible = false;
            btnNetx_ViewReport.Visible = false;
            txtViewReport_Admin.Visible = false;
            txtVieOrders_Admin.Visible = false;
            btnViewAll_Orders_Admin.Visible = false;



            btnSendMessage_Admin.Visible = true;
            txtMessage_Admin.Visible = true;
            txtTo_Admin.Visible = true;
            lblSendTo_Admin.Visible = true;
        }
      

        private void btnRecieveMessage_Click(object sender, EventArgs e)/**Veiw your message**/
        {
            txtViewPayments_Admin.Visible = false;
            btnNotPayed_Admin.Visible = false;
            btnRecievedPayment_admin.Visible = false;
            btnAllPayments_Admin.Visible = false;
            btnSendMessage_Admin.Visible =false;
            txtMessage_Admin.Visible = false;
            txtTo_Admin.Visible = false;
            lblSendTo_Admin.Visible = false;
            txtShowUsers_Admin.Visible = false;
            rBtnShowAllUsers_Admin.Visible = false;
            rBtnShowUserByID_Admin.Visible = false;
            lblID_UserManagementAdmin.Visible = false;
            txtID_UserManagementAdmin.Visible = false;
            btnShow_UManagementAdmin.Visible = false;
            txtEmail_AddUser.Visible = false;
            txtFirstName_AddUser.Visible = false;
            txtLastName_AddUser.Visible = false;
            txtID_AddUser.Visible = false;
            txtIDToDelete_RemoveUser.Visible = false;
            btnADDUser_Admin.Visible = false;
            btnRemoveUser_Admin.Visible = false;
            txtViewAssignments_ViewAssignments.Visible = false;
            btnNext_ViewAssignments.Visible = false;
            txtViewRequest_Admin.Visible = false;
            btnNext_ViewRequest.Visible = false;
            btnNetx_ViewReport.Visible = false;
            txtViewReport_Admin.Visible = false;
            txtVieOrders_Admin.Visible = false;
            btnViewAll_Orders_Admin.Visible = false;


            txtViewMessage_Admin.Visible = true;
            btnNextMessage_Admin.Visible = true;
            messages = admin.ViewMessage(admin.Id);
            index = 0;
        }
        
        private void btnNextMessage_Admin_Click(object sender, EventArgs e)/**see next message***/
        {


            while (index < messages.Count)
            {
                txtViewMessage_Admin.Text += messages[index++].ToString();
            }
        }
        public bool isOnlyDigit(string text)
        {

        private void btnSendMessage_Admin_Click(object sender, EventArgs e){
         
            foreach (char c in text)
            {
                if (c < '0' || c > '9')
                {
                    MessageBox.Show(this, "you need to enter numbers only!", "Closing", MessageBoxButtons.OK);
                    return false;
                }
            }
            return true;
        }
            
        private void btnSendMessage_Admin_Click(object sender, EventArgs e){
    
           string iD =  txtTo_Admin.Text;
           string text=txtMessage_Admin.Text;
           Boolean flag = true;

   

       
           string iD =  txtTo_Admin.Text;
            string text=txtMessage_Admin.Text;
           if( isOnlyDigit(txtTo_Admin.Text)==true)
           {
               int id = Int32.Parse( iD);
            if (id <= 999999 && id > 1000)
                admin.sendMessage(text, id);
            else
                MessageBox.Show(this,"The id you entered is not valid!", "Closing" );
           }
            btnSendMessage_Admin.Visible = false;
            txtMessage_Admin.Visible = false;
            txtTo_Admin.Visible = false;
            lblSendTo_Admin.Visible = false;
            txtViewMessage_Admin.Visible = false;
            btnNextMessage_Admin.Visible = false;
            txtViewPayments_Admin.Visible = false;
            btnNotPayed_Admin.Visible = false;
            btnRecievedPayment_admin.Visible = false;
            btnAllPayments_Admin.Visible = false;
            txtShowUsers_Admin.Visible = false;
            rBtnShowAllUsers_Admin.Visible = false;
            rBtnShowUserByID_Admin.Visible = false;
            lblID_UserManagementAdmin.Visible = false;
            txtID_UserManagementAdmin.Visible = false;
            btnShow_UManagementAdmin.Visible = false;
            txtEmail_AddUser.Visible = false;
            txtFirstName_AddUser.Visible = false;
            txtLastName_AddUser.Visible = false;
            txtID_AddUser.Visible = false;
            txtIDToDelete_RemoveUser.Visible = false;
            btnADDUser_Admin.Visible = false;
            btnRemoveUser_Admin.Visible = false;
            txtViewAssignments_ViewAssignments.Visible = false;
            btnNext_ViewAssignments.Visible = false;
            txtViewRequest_Admin.Visible = false;
            btnNext_ViewRequest.Visible = false;
            btnNetx_ViewReport.Visible = false;
            txtViewReport_Admin.Visible = false;
            txtVieOrders_Admin.Visible = false;
            btnViewAll_Orders_Admin.Visible = false;
            if (index == false)
            {
                btnSendMessage_Admin.Visible = true;
                txtMessage_Admin.Visible = true;
                txtTo_Admin.Visible = true;
                lblSendTo_Admin.Visible = true;
            }
           

        }

      /**********************************************View Payments*********************************************/
        private void btnViewPayments_Click(object sender, System.EventArgs e)
        {
            txtViewPayments_Admin.Visible = true;
            btnNotPayed_Admin.Visible = true;
            btnRecievedPayment_admin.Visible = true;
            btnAllPayments_Admin.Visible = true;


            btnSendMessage_Admin.Visible = false;
            txtMessage_Admin.Visible = false;
            txtTo_Admin.Visible = false;
            lblSendTo_Admin.Visible = false;
            txtViewMessage_Admin.Visible = false;
            btnNextMessage_Admin.Visible = false;
            txtShowUsers_Admin.Visible = false;
            rBtnShowAllUsers_Admin.Visible = false;
            rBtnShowUserByID_Admin.Visible = false;
            lblID_UserManagementAdmin.Visible = false;
            txtID_UserManagementAdmin.Visible = false;
            btnShow_UManagementAdmin.Visible = false;
            txtEmail_AddUser.Visible = false;
            txtFirstName_AddUser.Visible = false;
            txtLastName_AddUser.Visible = false;
            txtID_AddUser.Visible = false;
            txtIDToDelete_RemoveUser.Visible = false;
            btnADDUser_Admin.Visible = false;
            btnRemoveUser_Admin.Visible = false;
            txtViewAssignments_ViewAssignments.Visible = false;
            btnNext_ViewAssignments.Visible = false;
            txtViewRequest_Admin.Visible = false;
            btnNext_ViewRequest.Visible = false;
            btnNetx_ViewReport.Visible = false;
            txtViewReport_Admin.Visible = false;
            txtVieOrders_Admin.Visible = false;
            btnViewAll_Orders_Admin.Visible = false;


            payments = admin.ViewPayment();
            NotPayed = admin.remainingBills();
            index = 0;
        }


        private void btnRecievedPayment_admin_Click(object sender, EventArgs e)
        {
            txtViewPayments_Admin.Clear();
            while (index < payments.Count)
            { 
              txtViewPayments_Admin.Text += payments[index++].ToString();

            }
            
            index = 0;
        }
        private void btnNotPayed_Admin_Click(object sender, EventArgs e)
        {
            txtViewPayments_Admin.Clear();
            while (index < NotPayed.Count)
            {
                txtViewPayments_Admin.Text += NotPayed[index++].ToString();

            }
            index = 0;
        }
        private void btnAllPayments_Admin_Click(object sender, EventArgs e)
        {
            txtViewPayments_Admin.Clear();
            while (index < payments.Count)
            {
                txtViewPayments_Admin.Text += payments[index++].ToString();

            }

            index = 0;
            while (index < NotPayed.Count)
            {
                txtViewPayments_Admin.Text += NotPayed[index++].ToString();

            }
            index = 0;
        }
        /***********************************Show user Button*******************************************/
        private void button3_Click(object sender, System.EventArgs e)
        {
            txtViewPayments_Admin.Visible = false;
            btnNotPayed_Admin.Visible = false;
            btnRecievedPayment_admin.Visible = false;
            btnAllPayments_Admin.Visible = false;
            btnSendMessage_Admin.Visible = false;
            txtMessage_Admin.Visible = false;
            txtTo_Admin.Visible = false;
            lblSendTo_Admin.Visible = false;
            txtViewMessage_Admin.Visible = false;
            btnNextMessage_Admin.Visible = false;
            txtEmail_AddUser.Visible = false;
            txtFirstName_AddUser.Visible = false;
            txtLastName_AddUser.Visible = false;
            txtID_AddUser.Visible = false;
            txtIDToDelete_RemoveUser.Visible = false;
            btnADDUser_Admin.Visible = false;
            btnRemoveUser_Admin.Visible = false;
            txtViewAssignments_ViewAssignments.Visible = false;
            btnNext_ViewAssignments.Visible = false;
            txtViewRequest_Admin.Visible = false;
            btnNext_ViewRequest.Visible = false;
            btnNetx_ViewReport.Visible = false;
            txtViewReport_Admin.Visible = false;
            txtVieOrders_Admin.Visible = false;
            btnViewAll_Orders_Admin.Visible = false;


            txtShowUsers_Admin.Visible = true;
            rBtnShowAllUsers_Admin.Visible = true;
            rBtnShowUserByID_Admin.Visible = true;
            lblID_UserManagementAdmin.Visible = true;
            txtID_UserManagementAdmin.Visible = true;
            btnShow_UManagementAdmin.Visible = true;
            
           
            index = 0;

        }

        private void btnShow_UManagementAdmin_Click(object sender, EventArgs e)
        {
            int id=0;
            txtShowUsers_Admin.Clear();
            if (rBtnShowAllUsers_Admin.Checked) txtID_UserManagementAdmin.Text = "0";
            if(rBtnShowUserByID_Admin.Checked)
             id=Int32.Parse(txtID_UserManagementAdmin.Text);
            users = admin.printUsers(id);
            while (index < users.Count)
            {
                txtShowUsers_Admin.Text += users[index++].ToString();
            }
            index = 0;
            
           /* else
            {
                MessageBox.Show("No more reports!");
            }
           */
        }
       
        /*********************ADD/REMOVE USER*********************************************/
        private void btnAddRemoveUser_Admin_Click(object sender, EventArgs e)
        {
            txtViewPayments_Admin.Visible = false;
            btnNotPayed_Admin.Visible = false;
            btnRecievedPayment_admin.Visible = false;
            btnAllPayments_Admin.Visible = false;
            btnSendMessage_Admin.Visible = false;
            txtMessage_Admin.Visible = false;
            txtTo_Admin.Visible = false;
            lblSendTo_Admin.Visible = false;
            txtViewMessage_Admin.Visible = false;
            btnNextMessage_Admin.Visible = false;
            txtShowUsers_Admin.Visible = false;
            rBtnShowAllUsers_Admin.Visible = false;
            rBtnShowUserByID_Admin.Visible = false;
            lblID_UserManagementAdmin.Visible = false;
            txtID_UserManagementAdmin.Visible = false;
            btnShow_UManagementAdmin.Visible = false;
            txtViewAssignments_ViewAssignments.Visible = false;
            btnNext_ViewAssignments.Visible = false;
            txtViewRequest_Admin.Visible = false;
            btnNext_ViewRequest.Visible = false;
            btnNetx_ViewReport.Visible = false;
            txtViewReport_Admin.Visible = false;
            txtVieOrders_Admin.Visible = false;
            btnViewAll_Orders_Admin.Visible = false;


            txtEmail_AddUser.Visible = true;
            txtFirstName_AddUser.Visible = true;
            txtLastName_AddUser.Visible = true;
            txtID_AddUser.Visible = true;
            txtIDToDelete_RemoveUser.Visible = true;
            btnADDUser_Admin.Visible = true;
            btnRemoveUser_Admin.Visible = true;
        }
        private void btnRemoveUser_Admin_Click(object sender, EventArgs e)
        { int ok;

        int idToDelete=Int32.Parse(txtIDToDelete_RemoveUser.Text);
        ok = admin.removeUser(idToDelete);
            if(ok==1)
            {
                MessageBox.Show("used deleted");
            }
            else
            {
                MessageBox.Show("no such user");
            }
        }
        private void btnADDUser_Admin_Click(object sender, EventArgs e)

        {
            int ok;
           string email= txtEmail_AddUser.Text;
             string firstname=txtFirstName_AddUser.Text;
             string lastname=txtLastName_AddUser.Text;
           int id= Int32.Parse(txtID_AddUser.Text);
           User user = new User(firstname, lastname, id, 1, email, firstname, lastname,"User", 1, "null","null", "null","null");
           ok=admin.AddUser(user);
            if(ok==1)
            {
                MessageBox.Show("user sucssfuly added");
            }
            else
            {
                MessageBox.Show("sorry somting gone wr0ng");
            }

        }
       
        /**************************************View assignments*********************************/
        private void btnViewAssignments_Admin_Click(object sender, EventArgs e)
        {
            txtViewPayments_Admin.Visible = false;
            btnNotPayed_Admin.Visible = false;
            btnRecievedPayment_admin.Visible = false;
            btnAllPayments_Admin.Visible = false;
            btnSendMessage_Admin.Visible = false;
            txtMessage_Admin.Visible = false;
            txtTo_Admin.Visible = false;
            lblSendTo_Admin.Visible = false;
            txtViewMessage_Admin.Visible = false;
            btnNextMessage_Admin.Visible = false;
            txtShowUsers_Admin.Visible = false;
            rBtnShowAllUsers_Admin.Visible = false;
            rBtnShowUserByID_Admin.Visible = false;
            lblID_UserManagementAdmin.Visible = false;
            txtID_UserManagementAdmin.Visible = false;
            btnShow_UManagementAdmin.Visible = false;
            txtEmail_AddUser.Visible = false;
            txtFirstName_AddUser.Visible = false;
            txtLastName_AddUser.Visible = false;
            txtID_AddUser.Visible = false;
            txtIDToDelete_RemoveUser.Visible = false;
            btnADDUser_Admin.Visible = false;
            btnRemoveUser_Admin.Visible = false;
            txtViewRequest_Admin.Visible = false;
            btnNext_ViewRequest.Visible = false;
            btnNetx_ViewReport.Visible = false;
            txtViewReport_Admin.Visible = false;
            txtVieOrders_Admin.Visible = false;
            btnViewAll_Orders_Admin.Visible = false;

            txtViewAssignments_ViewAssignments.Visible = true;
            btnNext_ViewAssignments.Visible = true;
            assignments = admin.ViewAssignments();
            index = 0;

        }
        private void btnNext_ViewAssignments_Click(object sender, EventArgs e)//view all assignments
        {
            
            if (index < assignments.Count)
            {
                txtViewAssignments_ViewAssignments.Text = assignments[index++].ToString();
             

            }
            else
            {
                MessageBox.Show("No more assignments!");
            }
        }

        /*******************************************View Request*****************************************/
        private void btnViewRequest_Admin_Click(object sender, EventArgs e)//display the window of requests
        {
            txtViewPayments_Admin.Visible = false;
            btnNotPayed_Admin.Visible = false;
            btnRecievedPayment_admin.Visible = false;
            btnAllPayments_Admin.Visible = false;
            btnSendMessage_Admin.Visible = false;
            txtMessage_Admin.Visible = false;
            txtTo_Admin.Visible = false;
            lblSendTo_Admin.Visible = false;
            txtViewMessage_Admin.Visible = false;
            btnNextMessage_Admin.Visible = false;
            txtShowUsers_Admin.Visible = false;
            rBtnShowAllUsers_Admin.Visible = false;
            rBtnShowUserByID_Admin.Visible = false;
            lblID_UserManagementAdmin.Visible = false;
            txtID_UserManagementAdmin.Visible = false;
            btnShow_UManagementAdmin.Visible = false;
            txtEmail_AddUser.Visible = false;
            txtFirstName_AddUser.Visible = false;
            txtLastName_AddUser.Visible = false;
            txtID_AddUser.Visible = false;
            txtIDToDelete_RemoveUser.Visible = false;
            btnADDUser_Admin.Visible = false;
            btnRemoveUser_Admin.Visible = false;
            txtViewAssignments_ViewAssignments.Visible = false;
            btnNext_ViewAssignments.Visible = false;
            btnNetx_ViewReport.Visible = false;
            txtViewReport_Admin.Visible = false;
            txtVieOrders_Admin.Visible = false;
            btnViewAll_Orders_Admin.Visible = false;

          
            txtViewRequest_Admin.Visible = true;
            btnNext_ViewRequest.Visible = true;
            requests = admin.ViewRequest();
            index = 0;
        }

        private void btnNext_ViewRequest_Click(object sender, System.EventArgs e)
        {
            if (index < requests.Count)
            {
                txtViewRequest_Admin.Text = requests[index++].ToString();
           
            }
            else
            {
                MessageBox.Show("No more requests!");
            }
        }
       
        /**************************************************************************************************/
        private void btnViewReport_Admin_Click(object sender, EventArgs e)//diplay of reports widow
        {
            txtViewPayments_Admin.Visible = false;
            btnNotPayed_Admin.Visible = false;
            btnRecievedPayment_admin.Visible = false;
            btnAllPayments_Admin.Visible = false;
            btnSendMessage_Admin.Visible = false;
            txtMessage_Admin.Visible = false;
            txtTo_Admin.Visible = false;
            lblSendTo_Admin.Visible = false;
            txtViewMessage_Admin.Visible = false;
            btnNextMessage_Admin.Visible = false;
            txtShowUsers_Admin.Visible = false;
            rBtnShowAllUsers_Admin.Visible = false;
            rBtnShowUserByID_Admin.Visible = false;
            lblID_UserManagementAdmin.Visible = false;
            txtID_UserManagementAdmin.Visible = false;
            btnShow_UManagementAdmin.Visible = false;
            txtEmail_AddUser.Visible = false;
            txtFirstName_AddUser.Visible = false;
            txtLastName_AddUser.Visible = false;
            txtID_AddUser.Visible = false;
            txtIDToDelete_RemoveUser.Visible = false;
            btnADDUser_Admin.Visible = false;
            btnRemoveUser_Admin.Visible = false;
            txtViewAssignments_ViewAssignments.Visible = false;
            btnNext_ViewAssignments.Visible = false;
            txtViewRequest_Admin.Visible = false;
            btnNext_ViewRequest.Visible = false;
            txtVieOrders_Admin.Visible = false;
            btnViewAll_Orders_Admin.Visible = false;


            btnNetx_ViewReport.Visible = true;
            txtViewReport_Admin.Visible = true;
            reports = admin.ViewReport();
            index = 0;
        }
        private void btnNetx_ViewReport_Click(object sender, EventArgs e)//see the next report
        {
            if (index < reports.Count)
            {
                txtViewReport_Admin.Clear();
                txtViewReport_Admin.Text += reports[index++].ToString();

            }
            else
            {
                MessageBox.Show("No more reports!");
            }
        
        }

        private void btnViewOrders_Admin_Click(object sender, EventArgs e)
        {
            txtVieOrders_Admin.Visible=true;
            btnViewAll_Orders_Admin.Visible = true;

            txtViewPayments_Admin.Visible = false;
            btnNotPayed_Admin.Visible = false;
            btnRecievedPayment_admin.Visible = false;
            btnAllPayments_Admin.Visible = false;
            btnSendMessage_Admin.Visible = false;
            txtMessage_Admin.Visible = false;
            txtTo_Admin.Visible = false;
            lblSendTo_Admin.Visible = false;
            txtViewMessage_Admin.Visible = false;
            btnNextMessage_Admin.Visible = false;
            txtShowUsers_Admin.Visible = false;
            rBtnShowAllUsers_Admin.Visible = false;
            rBtnShowUserByID_Admin.Visible = false;
            lblID_UserManagementAdmin.Visible = false;
            txtID_UserManagementAdmin.Visible = false;
            btnShow_UManagementAdmin.Visible = false;
            txtEmail_AddUser.Visible = false;
            txtFirstName_AddUser.Visible = false;
            txtLastName_AddUser.Visible = false;
            txtID_AddUser.Visible = false;
            txtIDToDelete_RemoveUser.Visible = false;
            btnADDUser_Admin.Visible = false;
            btnRemoveUser_Admin.Visible = false;
            txtViewAssignments_ViewAssignments.Visible = false;
            btnNext_ViewAssignments.Visible = false;
            txtViewRequest_Admin.Visible = false;
            btnNext_ViewRequest.Visible = false;
            btnNetx_ViewReport.Visible = false;
            txtViewReport_Admin.Visible = false;


            orders = admin.printOrder();
            index = 0;
        }

        private void btnViewAll_Orders_Admin_Click(object sender, EventArgs e)
        {
            while (index < orders.Count)
            {
                txtVieOrders_Admin.Text += orders[index++].ToString();
            }
        }
      
    }
}

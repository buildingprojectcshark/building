﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Assignments
    {


        
        private int AssignmentID;
        private string text;
        private DateTime date;
        private bool isRoachGuy=false;//inicates in the assignment is calling a roachGuy
        private bool iscleanBuilding = false;//inicates in the assignment is cleannig the building
        private bool isthrowTrash = false;//inicates in the assignment is throw the trash
        private bool isfinishClean = false;//inicates in the assignment is finish cleanning the building

        public Assignments(string text)
        {
            
            this.text = text;
            this.date = DateTime.Now;
    
        }


        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }


        public bool IsRoachGuy//בנאי למשתנה המעיד אם ההודעה מסוג דוח
        {
            get { return this.isRoachGuy; }
            set { this.isRoachGuy = value; }

        }
        public bool IscleanBuilding//בנאי למשתנה המעיד האם ההודעה היא מסוג בקשה
        {
            get { return this.iscleanBuilding; }
            set { this.iscleanBuilding = value; }

        }

        public bool IsthrowTrash//בנאי למשתנה המעיד האם ההודעה היא מסוג בקשה
        {
            get { return this.isthrowTrash; }
            set { this.isthrowTrash = value; }

        }

        public bool IsfinishClean//בנאי למשתנה המעיד האם ההודעה היא מסוג בקשה
        {
            get { return this.isfinishClean; }
            set { this.isfinishClean = value; }

        }


        public int IdAssignment
        {
            get { return this.AssignmentID; }
            set { this.AssignmentID = value; }

        }

        public DateTime Date
        {
            get { return this.date; }
            set { this.date=value; }
        }

        public override string ToString()
        {
            return "Assignment number: " + this.AssignmentID + ")\r\n"
             + this.text + "\r\n is complited on date" + this.date + "\r\n";
        }

    }
}



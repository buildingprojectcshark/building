﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;

namespace WindowsFormsApplication1
{

    class DB
    {
        static string dbAddress = "Data Source=DataBase.sqlite; Version=3;";
        private SQLiteConnection m_dbConnection = new SQLiteConnection(dbAddress);
        private static DB instance = new DB();


        private List<Resident> users = new List<Resident>();
        private List<Message> messages = new List<Message>();
        private List<Payment> payments = new List<Payment>();
        private List<Order> orders = new List<Order>();
        private List<Assignments> Assignments = new List<Assignments>();
        private DB()
        {
            if (File.Exists("DataBase.sqlite"))
            {

                m_dbConnection.Open();
            }
            else
            {

                CreateDataBase();
            }

        }
        private void CreateDataBase()
        {

            m_dbConnection.Open();
            /******************************************************CREATION OF USERS TABLE****************************************************************/
            string sql = "CREATE TABLE users (firstname TEXT NOT NULL,"
            + "lastname TEXT NOT NULL,id INTEGER PRIMARY KEY NOT NULL,"
            + "telephone INTEGER  NULL,email TEXT NOT NULL,username TEXT NOT NULL,"
            + "password TEXT NOT NULL,tag TEXT  NULL,bildingAdress TEXT  NULL,"
            + "payStatus TEXT NULL, appartmentID INTEGER  NULL,"
            + "status TEXT  NULL,religiousShabat TEXT  NULL,paymentBills TEXT NULL,"
            + "totalPayBills TEXT NULL,calling TEXT NULL,seniority INTEGER NULL,"
            + "monthWorkDays INTEGER NULL,keysAmount INTEGER NULL,janitorEducation TEXT NULL,"
            + "janitorPension INTEGER NULL, janitorSickness INTEGER NULL)";
           
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            /******************************************************** initialization OF THE USERS TABALE****************************************************/
            sql = "INSERT INTO users (firstname,lastname,id,telephone,email,username,password,tag,bildingAdress,payStatus,totalPayBills,calling,seniority)"
            + "VALUES('alex','putin',1111,0505050,'pution@king.ru','alex','putin','Admin','dizingof 5','payed','1000','called painter',10)";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();

            /****************************************************************Jenitor*****************************************************************************/
            sql = "INSERT INTO users (firstname,lastname,id,telephone,email,username,password,tag,bildingAdress,payStatus,"
            + "monthWorkDays,keysAmount,janitorEducation,janitorPension,janitorSickness)"
            + " VALUES('Ben','Gilad',2222,054444444,'email@email.com','ben','gilad','Janitor','dizingof 5','getPayed',"
            + "20,10,'high school',5000,1)";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            /*****************************************************************General population******************************************************************/
            sql = "INSERT INTO users (firstname,lastname,id,telephone,email,username,password,tag,bildingAdress,payStatus,"
               + "appartmentID,status,religiousShabat,paymentBills)"
            + " VALUES('or','gershov',3333,0525250,'or@king.ru','or','gershov','User','dizingof 5','payed',1,'buyed','no','1000')";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            
            sql = "INSERT INTO users (firstname,lastname,id,telephone,email,username,password,tag,bildingAdress,payStatus,"
                + "appartmentID,status,religiousShabat,paymentBills)"
           + " VALUES('avi','levi',4444,123213,'avi@gmail.com','avi','levi','User','dizingof 5','need to pay',2,'rent','yes','5000')";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            
            sql = "INSERT INTO users (firstname,lastname,id,telephone,email,username,password,tag,bildingAdress,payStatus,"
                + "appartmentID,status,religiousShabat,paymentBills)"
           + " VALUES('maor','zdaka',5555,123213,'mango@gmail.com','maor','zdaka','User','dizingof 5','payed',3,'sublet','yes','4000')";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
           
            sql = "INSERT INTO users (firstname,lastname,id,telephone,email,username,password,tag,bildingAdress,payStatus,"
                + "appartmentID,status,religiousShabat,paymentBills)"
           + " VALUES('bob','marley',6666,0525250,'marly@gmail.com','bob','marley','User','dizingof 5','payed',4,'rent','yes','10000')";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            /***************************************************************END OF  initialization**********************************************************/
            /**************************************************************CREATING THE MASSAGE TABLE*******************************************************************/
            sql = "CREATE TABLE message (messageId INTEGER PRIMARY KEY NOT NULL,message TEXT NOT NULL,"
            + "date TEXT NOT NULL,fromUser INTEGER NOT NULL,toUser INTEGER NOT NULL,isMessage TEXT NULL,isReport TEXT NULL,isRequest TEXT NULL,isViewed TEXT NULL)";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            sql = "INSERT INTO message (messageId,message,date,fromUser,toUser,isMessage,isViewed) VALUES(1,'i am test message','16/01/2016',1111,2222,'Message','read')";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            /**************************************************************CREATING THE PAYMENTS TABLE*****************************************************************/
           sql = "CREATE TABLE payments (paymentId INTEGER PRIMARY KEY NOT NULL,sum INTEGER NOT NULL,"
                 + "date TEXT NOT NULL,fromUser INTEGER  NULL,toUser INTEGER  NULL)";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            sql = "INSERT INTO payments VALUES(144,5000,'16/01/2016',1111,2222)";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            /***************************************************************CREATING THE ORDERS TABLE****************************************************************/
           sql = "CREATE TABLE orders (OrderID INTEGER PRIMARY KEY NOT NULL,toOrder TEXT NOT NULL,date TEXT NOT NULL,"
              + "fromUser TEXT NOT NULL,toCompany TEXT NOT NULL,ordertype TEXT NOT NULL)";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            sql = "INSERT INTO orders VALUES(144,'burger','16/01/2016',1111,'yumanges','1')";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            /**************************************************************CREATING THE ASSIGMENT TABLE**************************************************************/
            sql = "CREATE TABLE assignments(AssignmentID INTEGER PRIMARY KEY  NOT NULL,assigment TEXT NOT NULL,date TEXT NOT NULL)";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            sql = "INSERT INTO assignments VALUES(144,'welcome jenitor','16/01/2016')";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            /**********************************************/
            m_dbConnection.Close();
        }

        DB dataBase = DB.GetInstance();


        public static DB GetInstance()
        {
            return instance;
        }
        //******************************************************************************************************************************//
        //*****************************************Resident metods*********************************************************************//
        

        /*
         * *
         * */
        public int AdddUser(User user)
        {
            bool userExist = false;
            int ok=0;//אינדקס אם הפעולה בוצעה או לא
            string sql = "SELECT * FROM USERS WHERE id =" + user.Id + "";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                userExist = true;
                
            }
            if (!userExist)
            {
               
                sql = "INSERT INTO users (firstname,lastname,id,telephone,email,username,password,tag,bildingAdress,payStatus,"
           + "appartmentID,status,religiousShabat,paymentBills)"
           + " VALUES('"+ user.FirstName +"','"+ user.LastName +"',"+ user.Id +"," + user.Telephone + ""
           + ",'" + user.Email + "','" + user.Username+ "','" + user.Password + "',"
           +"'User','" + user.BildingAdress + "','" + user.PayStatus + "'," + user.AppartmentID + ","
           + "'" + user.Status + "','" + user.ReligiousShabat + "','" + user.PaymentBills + "')";
        
           
                command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();
                ok = 1;
                return ok;
            }
            return ok;
        }

        public int RemoveUser(int userId)
        {
            int ok=0;//אינדקס אם הפעולה בוצעה או לא
            string sql = "SELECT * FROM USERS WHERE id =" + userId + "";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                sql = "DELETE FROM USERS WHERE id=" + userId + "";
                command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteReader();
                ok = 1;
                return ok;
            }
            return ok;
        }
        public Resident Login(string username, string password)
        {
            Resident userLogin = null;

            string sql = "SELECT * FROM USERS WHERE username ='" + username + "' AND password= '" + password + "'";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {

                string firstName = reader["username"].ToString();
                string lastName = reader["lastName"].ToString();
                int id = Int32.Parse(reader["id"].ToString());
                int telephone = Int32.Parse(reader["telephone"].ToString());
                string email = reader["email"].ToString();
                string Username = reader["username"].ToString();
                string Password = reader["Password"].ToString();
                string tag = reader["tag"].ToString();
                string bildingAdress = reader["bildingAdress"].ToString();
                string payStatus = reader["payStatus"].ToString();
                if (tag == "Admin")
                {
                    string totalPayBills = reader["totalPayBills"].ToString();
                    string calling = reader["calling"].ToString();
                    int seniority = Int32.Parse(reader["seniority"].ToString());
                    Resident admin = new Admin(firstName, lastName, id, telephone, email, Username, Password, tag
                    , totalPayBills, calling, "", "", seniority, bildingAdress);
                    userLogin = admin;
                    return userLogin;
                }
                if (tag == "User")
                {

                    string paymentBills = reader["paymentBills"].ToString();
                    string religiousShabat = reader["religiousShabat"].ToString();
                    string status = reader["status"].ToString();
                    int appartmentID = Int32.Parse(reader["appartmentID"].ToString());

                    Resident user = new User(firstName, lastName, id, telephone, email, Username, Password, tag, appartmentID
                        , status, religiousShabat, paymentBills, bildingAdress);
                    userLogin = user;
                    return userLogin;
                }
                if (tag == "Janitor")
                {
                    int monthWorkDays = Int32.Parse(reader["monthWorkDays"].ToString());
                    int keysAmount = Int32.Parse(reader["keysAmount"].ToString());
                    int janitorSickness = Int32.Parse(reader["janitorSickness"].ToString());
                    int janitorPension = Int32.Parse(reader["janitorPension"].ToString());
                    string janitorEducation = reader["janitorEducation"].ToString();
                    Resident janitor = new Janitor(firstName, lastName, id, telephone, email, Username, Password, tag
                        , monthWorkDays, keysAmount, janitorEducation, janitorPension, janitorSickness, bildingAdress);
                    userLogin = janitor;
                    return userLogin;
                }

            }
            return userLogin;
        }
        public List<Resident> PrintUsers(int Userid)
        {
            
            List<Resident> users = new List<Resident>();
            if (Userid != 0)/***********************print speciphic user*************************/
            {
                string sql = "SELECT * FROM USERS WHERE id =" + Userid + "";
                SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                SQLiteDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    string firstName = reader["username"].ToString();
                    string lastName = reader["lastName"].ToString();
                    int id = Int32.Parse(reader["id"].ToString());
                    int telephone = Int32.Parse(reader["telephone"].ToString());
                    string email = reader["email"].ToString();
                    string Username = reader["username"].ToString();
                    string Password = reader["Password"].ToString();
                    string tag = reader["tag"].ToString();
                    string bildingAdress = reader["bildingAdress"].ToString();
                    string payStatus = reader["payStatus"].ToString();

                    if (tag == "Admin")
                    {
                        string totalPayBills = reader["totalPayBills"].ToString();
                        string calling = reader["calling"].ToString();
                        int seniority = Int32.Parse(reader["seniority"].ToString());
                        Resident admin = new Admin(firstName, lastName, id, telephone, email, Username, Password, tag
                        , totalPayBills, calling, "", "", seniority, bildingAdress);
                        users.Add(admin);
                        return users;
                    }
                    if (tag == "User")
                    {

                        string paymentBills = reader["paymentBills"].ToString();
                        string religiousShabat = reader["religiousShabat"].ToString();
                        string status = reader["status"].ToString();
                        int appartmentID = Int32.Parse(reader["appartmentID"].ToString());

                        Resident user = new User(firstName, lastName, id, telephone, email, Username, Password, tag, appartmentID
                            , status, religiousShabat, paymentBills, bildingAdress);
                        users.Add(user);
                        return users;
                    }
                    if (tag == "Jenitor")
                    {
                        int monthWorkDays = Int32.Parse(reader["monthWorkDays"].ToString());
                        int keysAmount = Int32.Parse(reader["keysAmount"].ToString());
                        int janitorSickness = Int32.Parse(reader["janitorSickness"].ToString());
                        int janitorPension = Int32.Parse(reader["janitorPension"].ToString());
                        string janitorEducation = reader["janitorEducation"].ToString();
                        Resident janitor = new Janitor(firstName, lastName, id, telephone, email, Username, Password, tag
                            , monthWorkDays, keysAmount, janitorEducation, janitorPension, janitorSickness, bildingAdress);
                        users.Add(janitor);
                        return users;
                    }
                }
            }
            else/***************print all users***********************************/
            {
                string sql = "SELECT * FROM USERS ";
                SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    string firstName = reader["username"].ToString();
                    string lastName = reader["lastName"].ToString();
                    int id = Int32.Parse(reader["id"].ToString());
                    int telephone = Int32.Parse(reader["telephone"].ToString());
                    string email = reader["email"].ToString();
                    string Username = reader["username"].ToString();
                    string Password = reader["Password"].ToString();
                    string tag = reader["tag"].ToString();
                    string bildingAdress = reader["bildingAdress"].ToString();
                    string payStatus = reader["payStatus"].ToString();

                    if (tag == "Admin")
                    {
                        string totalPayBills = reader["totalPayBills"].ToString();
                        string calling = reader["calling"].ToString();
                        int seniority = Int32.Parse(reader["seniority"].ToString());
                        Resident admin = new Admin(firstName, lastName, id, telephone, email, Username, Password, tag
                        , totalPayBills, calling, "", "", seniority, bildingAdress);
                        users.Add(admin);
                     
                    }
                    if (tag == "User")
                    {

                        string paymentBills = reader["paymentBills"].ToString();
                        string religiousShabat = reader["religiousShabat"].ToString();
                        string status = reader["status"].ToString();
                        int appartmentID = Int32.Parse(reader["appartmentID"].ToString());

                        Resident user = new User(firstName, lastName, id, telephone, email, Username, Password, tag, appartmentID
                            , status, religiousShabat, paymentBills, bildingAdress);
                        users.Add(user);
                       
                    }
                    if (tag == "Jenitor")
                    {
                        int monthWorkDays = Int32.Parse(reader["monthWorkDays"].ToString());
                        int keysAmount = Int32.Parse(reader["keysAmount"].ToString());
                        int janitorSickness = Int32.Parse(reader["janitorSickness"].ToString());
                        int janitorPension = Int32.Parse(reader["janitorPension"].ToString());
                        string janitorEducation = reader["janitorEducation"].ToString();
                        Resident janitor = new Janitor(firstName, lastName, id, telephone, email, Username, Password, tag
                            , monthWorkDays, keysAmount, janitorEducation, janitorPension, janitorSickness, bildingAdress);
                        users.Add(janitor);
                      

                    }
                }
              

            }
            return users;
        }

        /******************************************************************************************************************************************/
        /********************************************Payment methods*********************************************************************************/

        public void MakePayment(int sum, int from)
        {
            int to = 0;
            int paymentId = 0;
            string sql = "SELECT * FROM payments WHERE  paymentId = (SELECT MAX(paymentId)  FROM payments)";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                paymentId = Int32.Parse(reader["paymentId"].ToString());
                paymentId ++;
               
            
                Payment pay = new Payment(sum, from, to);

                sql = "INSERT INTO payments VALUES(" + paymentId + "," + sum + ",'" + pay.Date + "',"+from +",1111 )";
                command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();
            }
        }
        /*****************************************/
        public List<Payment> ViewPayment()
        {
            List<Payment> payments = new List<Payment>();

            string sql = "SELECT * FROM PAYMENTS";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();

            while(reader.Read())
            {
                string date = reader["date"].ToString();
                int paymentId = Int32.Parse(reader["paymentId"].ToString());
                int sum = Int32.Parse(reader["sum"].ToString());
                int fromUser = Int32.Parse(reader["fromUser"].ToString());
                int toUser = Int32.Parse(reader["toUser"].ToString());
                Payment pay = new Payment(sum, fromUser, toUser);

                payments.Add(pay);
            }
            return payments;
        }
        /********************************************/
        public List<User> RemainingBills()/**********not finised********///
        {
            List<User> users = new List<User>();
            string sql = "SELECT * FROM USERS WHERE id not in (SELECT fromUser FROM payments)";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
              
                    string firstName = reader["username"].ToString();
                    string lastName = reader["lastName"].ToString();
                    int id = Int32.Parse(reader["id"].ToString());
                    int telephone = Int32.Parse(reader["telephone"].ToString());
                    string email = reader["email"].ToString();
                    string Username = reader["username"].ToString();
                    string Password = reader["Password"].ToString();
                    string tag = reader["tag"].ToString();
                    string bildingAdress = reader["bildingAdress"].ToString();
                    string payStatus = reader["payStatus"].ToString();                
                        string paymentBills = reader["paymentBills"].ToString();
                        string religiousShabat = reader["religiousShabat"].ToString();
                        string status = reader["status"].ToString();
                        //int appartmentID = Int32.Parse(reader["appartmentID"].ToString());
                        int appartmentID = 000;
                        User user = new User(firstName, lastName, id, telephone, email, Username, Password, tag, appartmentID
                            , status, religiousShabat, paymentBills, bildingAdress);
                        users.Add(user);
               
            }
            return users;
           
        }
        /******************************************************************************************************************************************/
        /********************************************order metods*********************************************************************************/

        public void Order(Order order)
        {
           
            int orderId = 0;
            string sql = "SELECT * FROM orders WHERE  OrderID = (SELECT MAX(OrderID)  FROM orders)";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                orderId = Int32.Parse(reader["OrderID"].ToString());
                orderId++;
                sql = "INSERT INTO orders VALUES(" + orderId + ",'" + order.Orders +"'"
                    + ",'" + order.Date + "', '"+order.From+"' ,'"+order.To+"','"+order.ServiceType+"')";      
                command = new SQLiteCommand(sql, m_dbConnection);      
                command.ExecuteNonQuery();
            }
        }



        public List<Order> PrintOrders()
        {
            List<Order> orders = new List<Order>();
            string sql = "SELECT * FROM orders ";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                
                string toOrder = reader["toOrder"].ToString();
                int orderId = Int32.Parse(reader["OrderID"].ToString());
                string date = reader["date"].ToString();
                string from = reader["fromUser"].ToString();
                string to = reader["toCompany"].ToString();
                string ordertype = reader["ordertype"].ToString();

                Order order= new Order(toOrder, from, to, ordertype);
                order.IdOrder = orderId;
                orders.Add(order);
            }
            return orders;
        }
        /***************************************message metods**********************************/
        /******************************************************************************************************************************************************/

        public void SendMessage(Message message)//need to compleate
        {//add a messege to db


            string sql = "SELECT * FROM message WHERE  messageId = (SELECT MAX(messageId)  FROM message)";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            int messageId = 0;
            if (reader.Read())
            {
                messageId = Int32.Parse(reader["messageId"].ToString());

               
                
                    messageId++;
                    sql = "INSERT INTO message(messageId,message,date,fromUser,toUser,isMessage,isViewed) VALUES"
                        + "(" + messageId + ",'" + message.Text + "'"
                        + ",'" + message.Date + "'," + message.From + "," + message.To + ",'isMessage','notRead')";
                    command = new SQLiteCommand(sql, m_dbConnection);
                    command.ExecuteNonQuery();


                
            }
        }
        /******************************************************************************************************************************************************/
        public void SendReport(Message report)//adding a report type massage to db
        {

            string sql = "SELECT * FROM USERS WHERE tag ='Admin'";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            int id=0;
            int reportID=0;
             if (reader.Read())
            {

                id = Int32.Parse(reader["id"].ToString());
             sql = "SELECT * FROM message WHERE messageId = (SELECT MAX(messageId)  FROM message)";
             command = new SQLiteCommand(sql, m_dbConnection);
             reader = command.ExecuteReader();
             if (reader.Read())
             {
                 reportID = Int32.Parse(reader["messageId"].ToString());
                 reportID++;
                sql = "INSERT INTO message (messageId,message,date,fromUser,toUser,isReport)"
                     + "VALUES(" + reportID + ",'" + report.Text + "','" + report.Date + "'," + id + "," + report.From + ",'isReport')";
                 command = new SQLiteCommand(sql, m_dbConnection);
                 command.ExecuteNonQuery();
             }
            }

        }
        /******************************************************************************************************************************************************/
        public void SendRequest(Message request)//adding a requset type massege to db
        {
            string sql = "SELECT * FROM USERS WHERE tag ='Admin'";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
          SQLiteDataReader reader = command.ExecuteReader();
            int id=0;
            int requstID = 0;
            if (reader.Read())
            {
                
               id = Int32.Parse(reader["id"].ToString());

            sql = "SELECT * FROM message WHERE messageId = (SELECT MAX(messageId)  FROM message)";
             command = new SQLiteCommand(sql, m_dbConnection);
          reader = command.ExecuteReader();
             if (reader.Read())
             {
                 requstID = Int32.Parse(reader["messageId"].ToString());
               requstID ++;
                 sql = "INSERT INTO message(messageId,message,date,fromUser,toUser,isRequest) VALUES"
                     + "(" + requstID + ",'" + request.Text + "','" + request.Date + "'," + id + "," + request.From + ",'isRequest')";
                 command = new SQLiteCommand(sql, m_dbConnection);
                 command.ExecuteNonQuery();
             }
           }
        }
        /***************************************************************************************************************************************/
        public void AddAssignments(Assignments Assignment)//הוספה של המטלות שבוצעו לרשימת המטלות
        {

            int assigmentID = 0;
            string sql = "SELECT * FROM assignments WHERE assignmentId = (SELECT MAX(assignmentId)  FROM assignments)";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                assigmentID = Int32.Parse(reader["assignmentId"].ToString());
                assigmentID++;
                sql = "INSERT INTO assignments VALUES(" + assigmentID + ",'" + Assignment.Text + "','" + Assignment.Date + "')";
                command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();
            }
        }
        /***************************************************************/
        public List<Message> ViewReport()//see all reports from db
        {
            List<Message> reports = new List<Message>();
            string sql = "SELECT * FROM message WHERE isReport = isReport";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                string text = reader["message"].ToString();
                int fromUser = Int32.Parse(reader["fromUser"].ToString());
                int toUser = Int32.Parse(reader["toUser"].ToString());
                Message report = new Message(text, fromUser, toUser);
                int requsetId = Int32.Parse(reader["messageId"].ToString());
                report.IdMessage = requsetId;
                reports.Add(report);

            }
            return reports;
          
        }
        /***************************************************************/
        public List<Message> ViewRequest()//see all request from db
        {
            List<Message> requsts = new List<Message>();
            string sql = "SELECT * FROM message WHERE isRequest = isRequest";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                int requsetId = Int32.Parse(reader["messageId"].ToString());
                string text = reader["message"].ToString();
                int fromUser = Int32.Parse(reader["fromUser"].ToString());
                int toUser = Int32.Parse(reader["toUser"].ToString());
                Message requst= new Message(text, fromUser, toUser);
                requst.IdMessage = requsetId;
                requsts.Add(requst);

            }
            return requsts;
        }
        /***************************************************************/
        public List<Assignments> ViewAssignments()//see all Assignments from db
        {
            List<Assignments> assignments = new List<Assignments>();
            string sql = "SELECT * FROM Assignments";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                //sql = "CREATE TABLE assignments(AssignmentID INTEGER PRIMARY KEY NOT NULL,assigment TEXT NOT NULL,date TEXT NOT NULL)";
                /*******Assignments assin = new Assignments();************/
                string assigment = reader["assigment"].ToString();
                string date = reader["date"].ToString();
                int AssignmentID = Int32.Parse(reader["AssignmentID"].ToString());
                Assignments asing = new Assignments(assigment);
                asing.IdAssignment = AssignmentID;
                assignments.Add(asing);
            }
            return assignments;

        }
        /***************************************************************/
        public List<Message> ViewMessage(int fromId)//see all Message from db/*********not compleate***************/
        {
            List< Message > messages= new List<Message>();
            string sql = "SELECT * FROM message WHERE toUser =" + fromId + "";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                 int messageId = Int32.Parse(reader["messageId"].ToString());
                sql = "UPDATE message " +
                     "SET isViewed = 'read'" +
                     " WHERE messageId = '" + messageId + "'";
                command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();

                string text = reader["message"].ToString();
                int fromUser = Int32.Parse(reader["fromUser"].ToString());
                int toUser = Int32.Parse(reader["toUser"].ToString());
                Message message = new Message(text, fromUser, toUser);

                message.IdMessage = messageId;
                messages.Add(message);

            }
            return messages;

        }


    }
}



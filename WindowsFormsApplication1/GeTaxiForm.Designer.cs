﻿namespace WindowsFormsApplication1
{
    partial class GeTaxiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GeTaxiForm));
            this.GeTaxi = new System.Windows.Forms.Label();
            this.iNeedTaxiTo = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.order = new System.Windows.Forms.Button();
            this.Back = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // GeTaxi
            // 
            this.GeTaxi.AutoSize = true;
            this.GeTaxi.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GeTaxi.Location = new System.Drawing.Point(12, 9);
            this.GeTaxi.Name = "GeTaxi";
            this.GeTaxi.Size = new System.Drawing.Size(74, 22);
            this.GeTaxi.TabIndex = 0;
            this.GeTaxi.Text = "GeTaxi";
            // 
            // iNeedTaxiTo
            // 
            this.iNeedTaxiTo.AutoSize = true;
            this.iNeedTaxiTo.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iNeedTaxiTo.Location = new System.Drawing.Point(9, 49);
            this.iNeedTaxiTo.Name = "iNeedTaxiTo";
            this.iNeedTaxiTo.Size = new System.Drawing.Size(120, 18);
            this.iNeedTaxiTo.TabIndex = 1;
            this.iNeedTaxiTo.Text = "I neet taxi to...";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 82);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(240, 20);
            this.textBox1.TabIndex = 2;
            // 
            // order
            // 
            this.order.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.order.Location = new System.Drawing.Point(254, 82);
            this.order.Name = "order";
            this.order.Size = new System.Drawing.Size(75, 23);
            this.order.TabIndex = 3;
            this.order.Text = "Order";
            this.order.UseVisualStyleBackColor = true;
            // 
            // Back
            // 
            this.Back.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back.Location = new System.Drawing.Point(254, 234);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(75, 23);
            this.Back.TabIndex = 4;
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = true;
            // 
            // GeTaxiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(341, 269);
            this.Controls.Add(this.Back);
            this.Controls.Add(this.order);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.iNeedTaxiTo);
            this.Controls.Add(this.GeTaxi);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GeTaxiForm";
            this.Text = "GeTaxi";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label GeTaxi;
        private System.Windows.Forms.Label iNeedTaxiTo;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button order;
        private System.Windows.Forms.Button Back;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    interface IMndatoryDetails
    {
        string LastName
        {
            get;
            set;
        }
        string FirstName
        {
            get;
            set;
        }
        string Email
        {
            get;
            set;
        }
    }
}

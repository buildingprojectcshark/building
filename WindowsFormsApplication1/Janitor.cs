﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Janitor : Resident//ISendMessage
    {
        DB dataBase = DB.GetInstance();
        private int monthWorkDays;
        private int keysAmount;
        private String janitorEducation;
        private int janitorPension;
        private int janitorSickness;

        public Janitor()
        {
        }

        public Janitor(string FirstName, string LastName,
             int Id, int Telephone, string Email, string username, string password, string tag, int monthWorkDays,
            int keysAmount, string janitorEducation, int janitorPension, int janitorSickness, string bildingAdress)
            : base(FirstName, LastName, Id, Telephone, Email, username, password, tag = "Janitor", bildingAdress)
        {
            this.monthWorkDays = monthWorkDays;
            this.keysAmount = keysAmount;
            this.janitorEducation = janitorEducation;
            this.janitorPension = janitorPension;
            this.janitorSickness = janitorSickness;
        }

       
      

      
        public int MonthWorkDays
        {
            get { return this.monthWorkDays; }
            set { this.monthWorkDays = value; }
        }
        public int KeysAmount
        {
            get { return this.keysAmount; }
            set { this.keysAmount = value; }
        }
        public string JanitorEducation
        {
            get { return this.janitorEducation; }
            set { this.janitorEducation = value; }
        }
        public int JanitorPension
        {
            get { return this.janitorPension; }
            set { this.janitorPension = value; }
        }
        public int JanitorSickness
        {
            get { return this.janitorSickness; }
            set { this.janitorSickness = value; }
        }

        public override  void sendMessage(string text, int to)//send a message
        {
            Message mess = new Message(text, this.Id, to);
            dataBase.SendMessage(mess);
        }


        public void cleanBuilding()
        {
          Assignments clean=new Assignments("cleaning the building");
          clean.IscleanBuilding = true;
          dataBase.AddAssignments(clean);
        }
        
        public void finishClean()
        {
            Assignments finishClean = new Assignments("finished cleaning the building");
            finishClean.IsfinishClean = true;
            dataBase.AddAssignments(finishClean);
        }

        public void callRoachGuy()
        {
            Assignments RoachGuy = new Assignments("calling the roach guy");
            RoachGuy.IsRoachGuy = true;
            dataBase.AddAssignments(RoachGuy);
         
        }
        public void throwTrash()
        {
            Assignments trash= new Assignments("the trash has been throwed");
            trash.IsthrowTrash = true;
            dataBase.AddAssignments(trash);         
           
        }
        public List<Message> ViewMessage(int fromId)
        {
            return dataBase.ViewMessage(fromId);

        }
        public override string ToString()
        {

            return base.ToString() + "\t\n amunt of keys" + this.keysAmount;
        }

    }
}


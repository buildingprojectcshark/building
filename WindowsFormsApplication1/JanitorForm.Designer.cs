﻿namespace WindowsFormsApplication1
{
    partial class JanitorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JanitorForm));
            this.HelloJanitor = new System.Windows.Forms.Label();
            this.btnCleanBuilding = new System.Windows.Forms.Button();
            this.btnFinishClean = new System.Windows.Forms.Button();
            this.btnCallRoachGuy = new System.Windows.Forms.Button();
            this.btnSendMessage = new System.Windows.Forms.Button();
            this.btnThrowTrash = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnStartCleaning_Janitor = new System.Windows.Forms.Button();
            this.btnFinishClean_Janitor = new System.Windows.Forms.Button();
            this.btnCallRoachGuy_Janitor = new System.Windows.Forms.Button();
            this.btnThrowTrash_Janitor = new System.Windows.Forms.Button();
            this.btnSend_Janitor = new System.Windows.Forms.Button();
            this.txtMessage_Janitor = new System.Windows.Forms.TextBox();
            this.lblTypeMessage_Janitor = new System.Windows.Forms.Label();
            this.lblTo_Janitor = new System.Windows.Forms.Label();
            this.txtTo_SendMessage_Janitor = new System.Windows.Forms.TextBox();
            this.btnRecieveMessage_Janitor = new System.Windows.Forms.Button();
            this.btnNext_RecieveMessage_Janitor = new System.Windows.Forms.Button();
            this.txtRecieveMessage_Janitor = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // HelloJanitor
            // 
            this.HelloJanitor.AutoSize = true;
            this.HelloJanitor.BackColor = System.Drawing.Color.Transparent;
            this.HelloJanitor.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HelloJanitor.Location = new System.Drawing.Point(-3, 9);
            this.HelloJanitor.Name = "HelloJanitor";
            this.HelloJanitor.Size = new System.Drawing.Size(125, 22);
            this.HelloJanitor.TabIndex = 0;
            this.HelloJanitor.Text = "Hello Janitor";
            // 
            // btnCleanBuilding
            // 
            this.btnCleanBuilding.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCleanBuilding.Location = new System.Drawing.Point(1, 56);
            this.btnCleanBuilding.Name = "btnCleanBuilding";
            this.btnCleanBuilding.Size = new System.Drawing.Size(130, 23);
            this.btnCleanBuilding.TabIndex = 1;
            this.btnCleanBuilding.Text = "Clean building";
            this.btnCleanBuilding.UseVisualStyleBackColor = true;
            this.btnCleanBuilding.Click += new System.EventHandler(this.btnCleanBuilding_Click);
            // 
            // btnFinishClean
            // 
            this.btnFinishClean.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinishClean.Location = new System.Drawing.Point(1, 85);
            this.btnFinishClean.Name = "btnFinishClean";
            this.btnFinishClean.Size = new System.Drawing.Size(130, 23);
            this.btnFinishClean.TabIndex = 2;
            this.btnFinishClean.Text = "Finish Clean";
            this.btnFinishClean.UseVisualStyleBackColor = true;
            this.btnFinishClean.Click += new System.EventHandler(this.btnFinishClean_Click);
            // 
            // btnCallRoachGuy
            // 
            this.btnCallRoachGuy.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCallRoachGuy.Location = new System.Drawing.Point(1, 114);
            this.btnCallRoachGuy.Name = "btnCallRoachGuy";
            this.btnCallRoachGuy.Size = new System.Drawing.Size(130, 23);
            this.btnCallRoachGuy.TabIndex = 3;
            this.btnCallRoachGuy.Text = "Call roach guy";
            this.btnCallRoachGuy.UseVisualStyleBackColor = true;
            this.btnCallRoachGuy.Click += new System.EventHandler(this.btnCallRoachGuy_Click);
            // 
            // btnSendMessage
            // 
            this.btnSendMessage.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSendMessage.Location = new System.Drawing.Point(1, 172);
            this.btnSendMessage.Name = "btnSendMessage";
            this.btnSendMessage.Size = new System.Drawing.Size(130, 23);
            this.btnSendMessage.TabIndex = 4;
            this.btnSendMessage.Text = "Send message";
            this.btnSendMessage.UseVisualStyleBackColor = true;
            this.btnSendMessage.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // btnThrowTrash
            // 
            this.btnThrowTrash.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThrowTrash.Location = new System.Drawing.Point(1, 143);
            this.btnThrowTrash.Name = "btnThrowTrash";
            this.btnThrowTrash.Size = new System.Drawing.Size(130, 23);
            this.btnThrowTrash.TabIndex = 5;
            this.btnThrowTrash.Text = "Throw trash";
            this.btnThrowTrash.UseVisualStyleBackColor = true;
            this.btnThrowTrash.Click += new System.EventHandler(this.btnThrowTrash_Click);
            // 
            // btnBack
            // 
            this.btnBack.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Location = new System.Drawing.Point(1, 256);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 7;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnStartCleaning_Janitor
            // 
            this.btnStartCleaning_Janitor.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnStartCleaning_Janitor.Location = new System.Drawing.Point(470, 78);
            this.btnStartCleaning_Janitor.Name = "btnStartCleaning_Janitor";
            this.btnStartCleaning_Janitor.Size = new System.Drawing.Size(135, 139);
            this.btnStartCleaning_Janitor.TabIndex = 8;
            this.btnStartCleaning_Janitor.Text = "Start Cleaning";
            this.btnStartCleaning_Janitor.UseVisualStyleBackColor = true;
            this.btnStartCleaning_Janitor.Visible = false;
            this.btnStartCleaning_Janitor.Click += new System.EventHandler(this.btnStartCleaning_Janitor_Click);
            // 
            // btnFinishClean_Janitor
            // 
            this.btnFinishClean_Janitor.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnFinishClean_Janitor.Location = new System.Drawing.Point(470, 78);
            this.btnFinishClean_Janitor.Name = "btnFinishClean_Janitor";
            this.btnFinishClean_Janitor.Size = new System.Drawing.Size(135, 139);
            this.btnFinishClean_Janitor.TabIndex = 9;
            this.btnFinishClean_Janitor.Text = "Finish Cleaning";
            this.btnFinishClean_Janitor.UseVisualStyleBackColor = true;
            this.btnFinishClean_Janitor.Visible = false;
            this.btnFinishClean_Janitor.Click += new System.EventHandler(this.btnFinishClean_Janitor_Click_1);
            // 
            // btnCallRoachGuy_Janitor
            // 
            this.btnCallRoachGuy_Janitor.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnCallRoachGuy_Janitor.Location = new System.Drawing.Point(470, 78);
            this.btnCallRoachGuy_Janitor.Name = "btnCallRoachGuy_Janitor";
            this.btnCallRoachGuy_Janitor.Size = new System.Drawing.Size(135, 139);
            this.btnCallRoachGuy_Janitor.TabIndex = 10;
            this.btnCallRoachGuy_Janitor.Text = "Call Roach Guy";
            this.btnCallRoachGuy_Janitor.UseVisualStyleBackColor = true;
            this.btnCallRoachGuy_Janitor.Visible = false;
            this.btnCallRoachGuy_Janitor.Click += new System.EventHandler(this.btnCallRoachGuy_Janitor_Click);
            // 
            // btnThrowTrash_Janitor
            // 
            this.btnThrowTrash_Janitor.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnThrowTrash_Janitor.Location = new System.Drawing.Point(470, 78);
            this.btnThrowTrash_Janitor.Name = "btnThrowTrash_Janitor";
            this.btnThrowTrash_Janitor.Size = new System.Drawing.Size(135, 139);
            this.btnThrowTrash_Janitor.TabIndex = 11;
            this.btnThrowTrash_Janitor.Text = "Throw Trash";
            this.btnThrowTrash_Janitor.UseVisualStyleBackColor = true;
            this.btnThrowTrash_Janitor.Visible = false;
            this.btnThrowTrash_Janitor.Click += new System.EventHandler(this.btnThrowTrash_Janitor_Click);
            // 
            // btnSend_Janitor
            // 
            this.btnSend_Janitor.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnSend_Janitor.Location = new System.Drawing.Point(428, 227);
            this.btnSend_Janitor.Name = "btnSend_Janitor";
            this.btnSend_Janitor.Size = new System.Drawing.Size(100, 23);
            this.btnSend_Janitor.TabIndex = 12;
            this.btnSend_Janitor.Text = "Send";
            this.btnSend_Janitor.UseVisualStyleBackColor = true;
            this.btnSend_Janitor.Visible = false;
            this.btnSend_Janitor.Click += new System.EventHandler(this.btnSend_Janitor_Click);
            // 
            // txtMessage_Janitor
            // 
            this.txtMessage_Janitor.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.txtMessage_Janitor.Location = new System.Drawing.Point(134, 56);
            this.txtMessage_Janitor.Multiline = true;
            this.txtMessage_Janitor.Name = "txtMessage_Janitor";
            this.txtMessage_Janitor.Size = new System.Drawing.Size(391, 161);
            this.txtMessage_Janitor.TabIndex = 13;
            this.txtMessage_Janitor.Visible = false;
            // 
            // lblTypeMessage_Janitor
            // 
            this.lblTypeMessage_Janitor.AutoSize = true;
            this.lblTypeMessage_Janitor.BackColor = System.Drawing.Color.Transparent;
            this.lblTypeMessage_Janitor.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.lblTypeMessage_Janitor.Location = new System.Drawing.Point(134, 38);
            this.lblTypeMessage_Janitor.Name = "lblTypeMessage_Janitor";
            this.lblTypeMessage_Janitor.Size = new System.Drawing.Size(135, 15);
            this.lblTypeMessage_Janitor.TabIndex = 14;
            this.lblTypeMessage_Janitor.Text = "Type Your Message:";
            this.lblTypeMessage_Janitor.Visible = false;
            // 
            // lblTo_Janitor
            // 
            this.lblTo_Janitor.AutoSize = true;
            this.lblTo_Janitor.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.lblTo_Janitor.Location = new System.Drawing.Point(392, 38);
            this.lblTo_Janitor.Name = "lblTo_Janitor";
            this.lblTo_Janitor.Size = new System.Drawing.Size(27, 15);
            this.lblTo_Janitor.TabIndex = 15;
            this.lblTo_Janitor.Text = "To:";
            this.lblTo_Janitor.Visible = false;
            // 
            // txtTo_SendMessage_Janitor
            // 
            this.txtTo_SendMessage_Janitor.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.txtTo_SendMessage_Janitor.Location = new System.Drawing.Point(425, 30);
            this.txtTo_SendMessage_Janitor.Name = "txtTo_SendMessage_Janitor";
            this.txtTo_SendMessage_Janitor.Size = new System.Drawing.Size(100, 23);
            this.txtTo_SendMessage_Janitor.TabIndex = 16;
            this.txtTo_SendMessage_Janitor.Visible = false;
            // 
            // btnRecieveMessage_Janitor
            // 
            this.btnRecieveMessage_Janitor.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnRecieveMessage_Janitor.Location = new System.Drawing.Point(1, 201);
            this.btnRecieveMessage_Janitor.Name = "btnRecieveMessage_Janitor";
            this.btnRecieveMessage_Janitor.Size = new System.Drawing.Size(130, 27);
            this.btnRecieveMessage_Janitor.TabIndex = 17;
            this.btnRecieveMessage_Janitor.Text = "Recieve message";
            this.btnRecieveMessage_Janitor.UseVisualStyleBackColor = true;
            this.btnRecieveMessage_Janitor.Click += new System.EventHandler(this.btnRecieveMessage_Janitor_Click);
            // 
            // btnNext_RecieveMessage_Janitor
            // 
            this.btnNext_RecieveMessage_Janitor.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnNext_RecieveMessage_Janitor.Location = new System.Drawing.Point(428, 227);
            this.btnNext_RecieveMessage_Janitor.Name = "btnNext_RecieveMessage_Janitor";
            this.btnNext_RecieveMessage_Janitor.Size = new System.Drawing.Size(100, 23);
            this.btnNext_RecieveMessage_Janitor.TabIndex = 18;
            this.btnNext_RecieveMessage_Janitor.Text = "Next";
            this.btnNext_RecieveMessage_Janitor.UseVisualStyleBackColor = true;
            this.btnNext_RecieveMessage_Janitor.Visible = false;
            this.btnNext_RecieveMessage_Janitor.Click += new System.EventHandler(this.btnNext_RecieveMessage_Janitor_Click);
            // 
            // txtRecieveMessage_Janitor
            // 
            this.txtRecieveMessage_Janitor.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.txtRecieveMessage_Janitor.Location = new System.Drawing.Point(134, 56);
            this.txtRecieveMessage_Janitor.Multiline = true;
            this.txtRecieveMessage_Janitor.Name = "txtRecieveMessage_Janitor";
            this.txtRecieveMessage_Janitor.ReadOnly = true;
            this.txtRecieveMessage_Janitor.Size = new System.Drawing.Size(391, 161);
            this.txtRecieveMessage_Janitor.TabIndex = 19;
            this.txtRecieveMessage_Janitor.Visible = false;
            // 
            // JanitorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(649, 357);
            this.Controls.Add(this.txtRecieveMessage_Janitor);
            this.Controls.Add(this.btnNext_RecieveMessage_Janitor);
            this.Controls.Add(this.btnRecieveMessage_Janitor);
            this.Controls.Add(this.txtTo_SendMessage_Janitor);
            this.Controls.Add(this.lblTo_Janitor);
            this.Controls.Add(this.lblTypeMessage_Janitor);
            this.Controls.Add(this.txtMessage_Janitor);
            this.Controls.Add(this.btnSend_Janitor);
            this.Controls.Add(this.btnThrowTrash_Janitor);
            this.Controls.Add(this.btnCallRoachGuy_Janitor);
            this.Controls.Add(this.btnFinishClean_Janitor);
            this.Controls.Add(this.btnStartCleaning_Janitor);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnThrowTrash);
            this.Controls.Add(this.btnSendMessage);
            this.Controls.Add(this.btnCallRoachGuy);
            this.Controls.Add(this.btnFinishClean);
            this.Controls.Add(this.btnCleanBuilding);
            this.Controls.Add(this.HelloJanitor);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "JanitorForm";
            this.Text = "Janitor";
            this.Load += new System.EventHandler(this.JanitorForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HelloJanitor;
        private System.Windows.Forms.Button btnCleanBuilding;
        private System.Windows.Forms.Button btnFinishClean;
        private System.Windows.Forms.Button btnCallRoachGuy;
        private System.Windows.Forms.Button btnSendMessage;
        private System.Windows.Forms.Button btnThrowTrash;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnStartCleaning_Janitor;
        private System.Windows.Forms.Button btnFinishClean_Janitor;
        private System.Windows.Forms.Button btnCallRoachGuy_Janitor;
        private System.Windows.Forms.Button btnThrowTrash_Janitor;
        private System.Windows.Forms.Button btnSend_Janitor;
        private System.Windows.Forms.TextBox txtMessage_Janitor;
        private System.Windows.Forms.Label lblTypeMessage_Janitor;
        private System.Windows.Forms.Label lblTo_Janitor;
        private System.Windows.Forms.TextBox txtTo_SendMessage_Janitor;
        private System.Windows.Forms.Button btnRecieveMessage_Janitor;
        private System.Windows.Forms.Button btnNext_RecieveMessage_Janitor;
        private System.Windows.Forms.TextBox txtRecieveMessage_Janitor;
    }
}
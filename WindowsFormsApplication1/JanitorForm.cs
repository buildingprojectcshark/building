﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
     partial class JanitorForm : Form
    {
        Janitor janitor;
        static int index = 0; // message counter
        List<Message> messages;
   
        public JanitorForm()
        {
            InitializeComponent();
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
        }
        public JanitorForm(Janitor janitor)
        {
            this.janitor = janitor;
            InitializeComponent();

        }
        // *****************************Added closing on X click******************************************************************/
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;

            // Confirm user wants to close
            switch (MessageBox.Show(this, "Are you sure you want to close?", "Closing", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    e.Cancel = true;
                    break;
                default:
                    break;
            }
        }
        /*********************************Opening Janitor form*********************************************************/
        private void JanitorForm_Load(object sender, EventArgs e)
        {

            HelloJanitor.Text = "Welcome " + janitor.FirstName.ToUpper() + " " + janitor.LastName.ToUpper();
   
        }

         /********************************Back Button click(Creates new Login Form)***************************/
        private void btnBack_Click(object sender, EventArgs e)
        {
            LoginForm back = new LoginForm();
            this.Hide();
            back.Show();
            this.Owner = back;
        }

         /***********************************Send And Recieve Messeges**********************************/
        private void btnSendMessage_Click(object sender, EventArgs e)
        {
            btnFinishClean_Janitor.Visible = false;
            btnCallRoachGuy_Janitor.Visible = false;
            btnThrowTrash_Janitor.Visible = false;
            btnStartCleaning_Janitor.Visible = false;
            txtRecieveMessage_Janitor.Visible = false;
            btnNext_RecieveMessage_Janitor.Visible = false;

            lblTo_Janitor.Visible = true;
            txtTo_SendMessage_Janitor.Visible = true;
            btnSend_Janitor.Visible = true;
            lblTypeMessage_Janitor.Visible = true;
            txtMessage_Janitor.Visible = true;  
        }

        private void btnRecieveMessage_Janitor_Click(object sender, EventArgs e)
        {
            btnFinishClean_Janitor.Visible = false;
            btnCallRoachGuy_Janitor.Visible = false;
            btnThrowTrash_Janitor.Visible = false;
            btnStartCleaning_Janitor.Visible = false;
            lblTo_Janitor.Visible = false;
            txtTo_SendMessage_Janitor.Visible = false;
            btnSend_Janitor.Visible = false;
            lblTypeMessage_Janitor.Visible = false;
            txtMessage_Janitor.Visible = false;  


            txtRecieveMessage_Janitor.Visible = true;
            btnNext_RecieveMessage_Janitor.Visible = true;
            messages = janitor.ViewMessage(janitor.Id);
        }

        private void btnSend_Janitor_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(txtTo_SendMessage_Janitor.Text);
            string text = txtMessage_Janitor.Text;
            janitor.sendMessage(text, id);
            txtTo_SendMessage_Janitor.Clear();
            txtMessage_Janitor.Clear();
        }

        private void btnNext_RecieveMessage_Janitor_Click(object sender, EventArgs e)
        {
            while (index < messages.Count)
            {
                txtRecieveMessage_Janitor.Text += messages[index++].ToString();
            }
        }

         /***************************Clean Building Button And Methods**********************************/
        private void btnCleanBuilding_Click(object sender, EventArgs e)
        {
            btnFinishClean_Janitor.Visible = false;
            btnCallRoachGuy_Janitor.Visible = false;
            btnThrowTrash_Janitor.Visible = false;
            btnSend_Janitor.Visible = false;
            lblTypeMessage_Janitor.Visible = false;
            txtMessage_Janitor.Visible = false;
            lblTo_Janitor.Visible = false;
            txtTo_SendMessage_Janitor.Visible = false;
            txtRecieveMessage_Janitor.Visible = false;
            btnNext_RecieveMessage_Janitor.Visible = false;


            btnStartCleaning_Janitor.Visible = true;

        }

        private void btnFinishClean_Click(object sender, EventArgs e)
        {
            btnStartCleaning_Janitor.Visible = false;
            btnCallRoachGuy_Janitor.Visible = false;
            btnThrowTrash_Janitor.Visible = false;
            btnSend_Janitor.Visible = false;
            lblTypeMessage_Janitor.Visible = false;
            txtMessage_Janitor.Visible = false;
            lblTo_Janitor.Visible = false;
            txtTo_SendMessage_Janitor.Visible = false;
            txtRecieveMessage_Janitor.Visible = false;
            btnNext_RecieveMessage_Janitor.Visible = false;
            
            btnFinishClean_Janitor.Visible = true;


        
        }

        private void btnStartCleaning_Janitor_Click(object sender, EventArgs e)//start
        {
            janitor.cleanBuilding();

            //***************** Pop-up message of janitor cleaning ******************************* **
            base.OnClick(e);
            switch (MessageBox.Show(this, "The Janitor is cleaning the building.", "Finished cleaning", MessageBoxButtons.OK))
            {
                case DialogResult.No:

                    break;
                default:
                    break;
            }
        }

        private void btnFinishClean_Janitor_Click_1(object sender, EventArgs e)//finish
        {
            janitor.finishClean();

            //***************** Pop-up message of janitor cleaning ******************************* **
            base.OnClick(e);
            switch (MessageBox.Show(this, "The Janitor finished cleaning the building.", "Cleaning", MessageBoxButtons.OK))
            {
                case DialogResult.No:

                    break;
                default:
                    break;
            }
        }

        /***************************CallRoach Guy Button And Methods**********************************/
        private void btnCallRoachGuy_Click(object sender, EventArgs e)
        {
            btnStartCleaning_Janitor.Visible = false;
            btnFinishClean_Janitor.Visible = false;
            btnThrowTrash_Janitor.Visible = false;
            btnSend_Janitor.Visible = false;
            lblTypeMessage_Janitor.Visible = false;
            txtMessage_Janitor.Visible = false;
            lblTo_Janitor.Visible = false;
            txtTo_SendMessage_Janitor.Visible = false;
            txtRecieveMessage_Janitor.Visible = false;
            btnNext_RecieveMessage_Janitor.Visible = false;
            
            
            btnCallRoachGuy_Janitor.Visible = true;
        }
        
         private void btnCallRoachGuy_Janitor_Click(object sender, EventArgs e)
        {
            janitor.callRoachGuy();

            //***************** Pop-up message of janitor calling roach guy ******************************* **
            base.OnClick(e);
            switch (MessageBox.Show(this, "The Janitor is calling for Roach-support.", "Roach Killer", MessageBoxButtons.OK))
            {
                case DialogResult.No:

                    break;
                default:
                    break;
            }
        }


         /***************************CallRoach Guy Button And Methods**********************************/
        private void btnThrowTrash_Click(object sender, EventArgs e)
        {
            btnStartCleaning_Janitor.Visible = false;
            btnFinishClean_Janitor.Visible = false;
            btnCallRoachGuy_Janitor.Visible = false;
            btnSend_Janitor.Visible = false;
            lblTypeMessage_Janitor.Visible = false;
            txtMessage_Janitor.Visible = false;
            lblTo_Janitor.Visible = false;
            txtTo_SendMessage_Janitor.Visible = false;
            txtRecieveMessage_Janitor.Visible = false;
            btnNext_RecieveMessage_Janitor.Visible = false;
            
            btnThrowTrash_Janitor.Visible = true;
        }

        private void btnThrowTrash_Janitor_Click(object sender, EventArgs e)
        {
            janitor.throwTrash();

            //***************** Pop-up message of janitor throwing out the trash ******************************* **
            base.OnClick(e);
            switch (MessageBox.Show(this, "The Janitor is throwing away the trash.", "Something smells here...", MessageBoxButtons.OK))
            {
                case DialogResult.No:

                    break;
                default:
                    break;
            }
        }
    }
}

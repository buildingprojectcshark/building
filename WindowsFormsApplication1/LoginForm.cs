﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApplication1
{
    public partial class LoginForm : Form
    {
        
        DB dataBase = DB.GetInstance();

        public LoginForm()
        {
            InitializeComponent();
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
        }


        private void btnLogin_Click_1(object sender, EventArgs e)
        {
            //string username = txtUserName.Text;
           // string password = txtPassword.Text;

            string username = "alex";
            string password = "putin";

            Resident resident = dataBase.Login(username, password);



            if (resident != null)
            {
                if (resident.Tag == "Admin")
                {
                    Admin admin = (Admin)resident;
                    this.Hide();
                    AdminForm aForm = new AdminForm(admin);             
                    aForm.Show();
                    this.Owner = aForm;
                   
                }

                if (resident.Tag == "User")
                {
                    User user = (User)resident;
                    this.Hide();
                    UserForm uForm = new UserForm(user);
                    uForm.Show();
                    this.Owner = uForm;
                }

                if (resident.Tag == "Janitor")
                {
                    Janitor janitor = (Janitor)resident;
                    this.Hide();
                    JanitorForm jForm = new JanitorForm(janitor);
                    jForm.Show();
                    this.Owner = jForm;
                }
            }
           
            else
            {
                MessageBox.Show("There was a problem finding you, try again");
                txtUserName.Text = null;
                txtPassword.Text = null;
               
            }
        
        }

    }


}


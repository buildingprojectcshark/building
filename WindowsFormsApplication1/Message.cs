﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{

    public class Message
    {
        
        private int MessageID;
        private string text;
        private DateTime date;
        private int from;
        private int to;
        private bool isReport=false;//משתנה אשר יעיד האם ההודעה היא מסוג דוח
        private bool isRequest = false;//משתנה אשר יעיד אם הבקשה היא מסוג בקשה


        public Message(string text, int from, int to)
        {
            
            this.text = text;
            this.date = DateTime.Now;
            this.from = from;
            this.to = to;
        }




        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }

        public int From
        {
            get { return this.from; }
        }

        public int To
        {
            get { return this.to; }
            set { this.to = value; }

        }
        public bool IsReport//בנאי למשתנה המעיד אם ההודעה מסוג דוח
        {
            get { return this.isReport; }
            set { this.isReport = value; }

        }
        public bool IsRequest//בנאי למשתנה המעיד האם ההודעה היא מסוג בקשה
        {
            get { return this.isRequest; }
            set { this.isRequest = value; }

        }
        public int IdMessage
        {
            get { return this.MessageID; }
            set { this.MessageID = value; }
        }

        public DateTime Date
        {
            get { return this.date; }
        }

        public override string ToString()
        {
            return "\nMessage Number:" + this.MessageID + " \nFrom:" + this.from + " \nTo:" 
                    + this.to +"\r\nRecieved On:" + this.date + "\r\nText:\r\n" + this.text+
                    "\r\n**********************************************************\r\n\r\n";
        }
       
    }
}


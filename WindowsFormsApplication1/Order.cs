﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{

    class Order//calls of order food or a texi
    {
        private int idOrder;
        private string ordertxt;
        private DateTime date;
        private string from;
        private string to;
        private string serviceType;

        public Order(string ordertxt, string from, string to, string serviceType)
        {
            this.ordertxt = ordertxt;
            this.date = DateTime.Now;
            this.from = from;
            this.to = to;
            this.serviceType = serviceType;
        }

        public string Orders
        {
            get { return this.ordertxt; }
            set { this.ordertxt = value; }
        }

        public string From
        {
            get { return this.from; }
        }

        public string To
        {
            get { return this.to; }
        }

        public int IdOrder
        {
            get { return this.idOrder; }
            set { this.idOrder = value; }
        }
        public string ServiceType { get { return this.serviceType; } }

        public DateTime Date
        {
            get { return this.date; }
        }

        public override string ToString()
        {
            return "Order number: " + this.idOrder + "\n\t\n\t" + "From: " + this.from +
                "\n\t\n\tTo: " + this.to + " \n\t\n\t\n\torder type: " + this.serviceType + "\n\t\n\t\n\t the order: \n\t\n\t\n\t" +
                this.ordertxt + "\n\t\n\t\n\t" + this.date + "\n\t*********************************************************\n\t\n\t";
        }

    }
}


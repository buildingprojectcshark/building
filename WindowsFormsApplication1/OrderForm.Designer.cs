﻿namespace WindowsFormsApplication1
{
    partial class OrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderForm));
            this.btnBack = new System.Windows.Forms.Button();
            this.Order = new System.Windows.Forms.Label();
            this.pleaseChoose = new System.Windows.Forms.Label();
            this.btnTaxi = new System.Windows.Forms.Button();
            this.btnOrderFood = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Location = new System.Drawing.Point(247, 232);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 0;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // Order
            // 
            this.Order.AutoSize = true;
            this.Order.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Order.Location = new System.Drawing.Point(12, 18);
            this.Order.Name = "Order";
            this.Order.Size = new System.Drawing.Size(64, 22);
            this.Order.TabIndex = 1;
            this.Order.Text = "Order";
            // 
            // pleaseChoose
            // 
            this.pleaseChoose.AutoSize = true;
            this.pleaseChoose.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pleaseChoose.Location = new System.Drawing.Point(13, 63);
            this.pleaseChoose.Name = "pleaseChoose";
            this.pleaseChoose.Size = new System.Drawing.Size(130, 18);
            this.pleaseChoose.TabIndex = 2;
            this.pleaseChoose.Text = "Please choose:";
            // 
            // btnTaxi
            // 
            this.btnTaxi.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTaxi.Location = new System.Drawing.Point(58, 117);
            this.btnTaxi.Name = "btnTaxi";
            this.btnTaxi.Size = new System.Drawing.Size(75, 23);
            this.btnTaxi.TabIndex = 3;
            this.btnTaxi.Text = "GeTaxi";
            this.btnTaxi.UseVisualStyleBackColor = true;
            this.btnTaxi.Click += new System.EventHandler(this.btnTaxi_Click);
            // 
            // btnOrderFood
            // 
            this.btnOrderFood.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrderFood.Location = new System.Drawing.Point(209, 117);
            this.btnOrderFood.Name = "btnOrderFood";
            this.btnOrderFood.Size = new System.Drawing.Size(75, 23);
            this.btnOrderFood.TabIndex = 4;
            this.btnOrderFood.Text = "Order Food";
            this.btnOrderFood.UseVisualStyleBackColor = true;
            this.btnOrderFood.Click += new System.EventHandler(this.btnOrderFood_Click);
            // 
            // OrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(341, 269);
            this.Controls.Add(this.btnOrderFood);
            this.Controls.Add(this.btnTaxi);
            this.Controls.Add(this.pleaseChoose);
            this.Controls.Add(this.Order);
            this.Controls.Add(this.btnBack);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OrderForm";
            this.Text = "Order";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label Order;
        private System.Windows.Forms.Label pleaseChoose;
        private System.Windows.Forms.Button btnTaxi;
        private System.Windows.Forms.Button btnOrderFood;
    }
}
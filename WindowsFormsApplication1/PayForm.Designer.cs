﻿namespace WindowsFormsApplication1
{
    partial class PayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PayForm));
            this.Pay = new System.Windows.Forms.Label();
            this.EnterSumToPay = new System.Windows.Forms.Label();
            this.txtBoxSumToPay = new System.Windows.Forms.TextBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnPayNow = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Pay
            // 
            this.Pay.AutoSize = true;
            this.Pay.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pay.Location = new System.Drawing.Point(12, 18);
            this.Pay.Name = "Pay";
            this.Pay.Size = new System.Drawing.Size(47, 24);
            this.Pay.TabIndex = 0;
            this.Pay.Text = "Pay";
            this.Pay.Click += new System.EventHandler(this.Pay_Click);
            // 
            // EnterSumToPay
            // 
            this.EnterSumToPay.AutoSize = true;
            this.EnterSumToPay.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EnterSumToPay.Location = new System.Drawing.Point(12, 57);
            this.EnterSumToPay.Name = "EnterSumToPay";
            this.EnterSumToPay.Size = new System.Drawing.Size(170, 22);
            this.EnterSumToPay.TabIndex = 1;
            this.EnterSumToPay.Text = "Enter sum to pay:";
            // 
            // txtBoxSumToPay
            // 
            this.txtBoxSumToPay.BackColor = System.Drawing.Color.LightBlue;
            this.txtBoxSumToPay.Location = new System.Drawing.Point(16, 82);
            this.txtBoxSumToPay.Name = "txtBoxSumToPay";
            this.txtBoxSumToPay.Size = new System.Drawing.Size(100, 20);
            this.txtBoxSumToPay.TabIndex = 2;
            // 
            // btnBack
            // 
            this.btnBack.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Location = new System.Drawing.Point(179, 221);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 3;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnPayNow
            // 
            this.btnPayNow.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPayNow.Location = new System.Drawing.Point(260, 221);
            this.btnPayNow.Name = "btnPayNow";
            this.btnPayNow.Size = new System.Drawing.Size(75, 23);
            this.btnPayNow.TabIndex = 4;
            this.btnPayNow.Text = "Pay now";
            this.btnPayNow.UseVisualStyleBackColor = true;
            this.btnPayNow.Click += new System.EventHandler(this.btnPayNow_Click);
            // 
            // PayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(341, 269);
            this.Controls.Add(this.btnPayNow);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.txtBoxSumToPay);
            this.Controls.Add(this.EnterSumToPay);
            this.Controls.Add(this.Pay);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PayForm";
            this.Text = "Pay";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void btnPayNow_Click(object sender, System.EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void Pay_Click(object sender, System.EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Label Pay;
        private System.Windows.Forms.Label EnterSumToPay;
        private System.Windows.Forms.TextBox txtBoxSumToPay;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnPayNow;
    }
}
﻿namespace WindowsFormsApplication1
{
    partial class PayValidationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PayValidationForm));
            this.PayValidation = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // PayValidation
            // 
            this.PayValidation.AutoSize = true;
            this.PayValidation.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PayValidation.Location = new System.Drawing.Point(12, 9);
            this.PayValidation.Name = "PayValidation";
            this.PayValidation.Size = new System.Drawing.Size(151, 24);
            this.PayValidation.TabIndex = 0;
            this.PayValidation.Text = "Pay validation";
            // 
            // PayValidationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(341, 269);
            this.Controls.Add(this.PayValidation);
            this.DoubleBuffered = true;
            this.Name = "PayValidationForm";
            this.Text = "Pay Validation";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label PayValidation;
    }
}
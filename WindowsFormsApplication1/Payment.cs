﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{

    class Payment
    {

        private static int count = 0;
        private int idPayment;
        private int sum;
        private DateTime date;
        private int from;
        private int to;

        public Payment(int sum, int from, int to)
        {
            this.idPayment = count++;
            this.sum = sum;
            this.date = DateTime.Now;
            this.from = from;
            this.to = to;
        }

        public Payment(){}

        public int Sum
        {
            get { return this.sum; }
            set { this.sum = value; }
        }

        public int From
        {
            get { return this.from; }
        }

        public int To
        {
            get { return this.to; }
        }

        public int IdPayment
        {
            get { return this.idPayment; }
            set { this.idPayment = value; }
        }

        public DateTime Date
        {
            get { return this.date; }
        }

        public override string ToString()
        {
            return "\nPayment number: " + this.idPayment + "\t\n" + "From: " + this.from + "\t\nTo: " + this.to + "\t\npayment amount:\n" + this.sum + "\t\n" + this.date + "\t\n";
        }

    }
}


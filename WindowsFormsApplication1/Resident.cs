﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{

    abstract class Resident : IMndatoryDetails, ISendMessage // מחלקת האב
    {
        // public enum Permission { Admin=1, User, Janitor}

        DB dataBase = DB.GetInstance();
        private string firstName;
        private string lastName;
        private int id;
        private int telephone;
        private string email;
        private string username;
        private string password;
        private string tag;
        private string bildingAdress;
        private bool payStatus = false;
        public Resident()
        {
        }
        public Resident(string firstName, string lastName,
            int id, int telephone, string email, string username, string password, string tag, string bildingAdress)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.id = id;
            this.telephone = telephone;
            this.email = email;
            this.username = username;
            this.password = password;
            this.tag = tag;
            this.bildingAdress = bildingAdress;

            //  this.permission = permission;
        }
        public string FirstName
        {
            get { return this.firstName; }
            set { this.firstName = value; }
        }
        public string LastName
        {
            get { return this.lastName; }
            set { this.lastName = value; }
        }
        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }
        public int Telephone
        {
            get { return this.telephone; }
            set { this.telephone = value; }
        }
        public string Email
        {
            get { return this.email; }
            set { this.email = value; }
        }
        public string Username
        {
            get { return this.username; }
            set { this.username = value; }
        }
        public string Password
        {
            get { return this.password; }
            set { this.password = value; }
        }
        public string Tag
        {
            get { return this.tag; }
            set { this.tag = value; }
        }
        public string BildingAdress
        {
            get { return this.bildingAdress; }
            set { this.bildingAdress = value; }
        }
        public bool PayStatus
        {
            get { return this.payStatus; }
            set { this.payStatus = value; }

        }
        //public int Permission
        //{
        //    get { return this.permission; }
        //}
        public override string ToString()
        {
            
            return "\nressident:" + this.firstName + " " + this.lastName + "\n   id:" + this.id + "   \t\ntelephone no#:" + this.telephone + "    \r\nEmail:" + this.email;
        }
        public virtual void sendMessage(string text, int to)
        {

        }
        public virtual void printMenu() { }
    }
}


﻿namespace WindowsFormsApplication1
{
    partial class SendMessageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SendMessageForm));
            this.WriteMessage = new System.Windows.Forms.Label();
            this.ToBoxMessage = new System.Windows.Forms.TextBox();
            this.btnSendMessage = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.txtTo = new System.Windows.Forms.Label();
            this.MessageTextBox = new System.Windows.Forms.TextBox();
            this.textMessage = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // WriteMessage
            // 
            this.WriteMessage.AutoSize = true;
            this.WriteMessage.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WriteMessage.Location = new System.Drawing.Point(2, 18);
            this.WriteMessage.Name = "WriteMessage";
            this.WriteMessage.Size = new System.Drawing.Size(143, 22);
            this.WriteMessage.TabIndex = 0;
            this.WriteMessage.Text = "Send Message";
            // 
            // ToBoxMessage
            // 
            this.ToBoxMessage.BackColor = System.Drawing.Color.LightBlue;
            this.ToBoxMessage.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToBoxMessage.Location = new System.Drawing.Point(93, 78);
            this.ToBoxMessage.Multiline = true;
            this.ToBoxMessage.Name = "ToBoxMessage";
            this.ToBoxMessage.Size = new System.Drawing.Size(212, 28);
            this.ToBoxMessage.TabIndex = 1;
            // 
            // btnSendMessage
            // 
            this.btnSendMessage.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSendMessage.Location = new System.Drawing.Point(173, 234);
            this.btnSendMessage.Name = "btnSendMessage";
            this.btnSendMessage.Size = new System.Drawing.Size(75, 23);
            this.btnSendMessage.TabIndex = 2;
            this.btnSendMessage.Text = "Send";
            this.btnSendMessage.UseVisualStyleBackColor = true;
            // 
            // btnBack
            // 
            this.btnBack.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Location = new System.Drawing.Point(254, 234);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 3;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // txtTo
            // 
            this.txtTo.AutoSize = true;
            this.txtTo.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTo.Location = new System.Drawing.Point(3, 77);
            this.txtTo.Name = "txtTo";
            this.txtTo.Size = new System.Drawing.Size(33, 18);
            this.txtTo.TabIndex = 4;
            this.txtTo.Text = "To:";
            // 
            // MessageTextBox
            // 
            this.MessageTextBox.BackColor = System.Drawing.Color.LightBlue;
            this.MessageTextBox.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MessageTextBox.Location = new System.Drawing.Point(93, 112);
            this.MessageTextBox.Multiline = true;
            this.MessageTextBox.Name = "MessageTextBox";
            this.MessageTextBox.Size = new System.Drawing.Size(212, 116);
            this.MessageTextBox.TabIndex = 5;
            // 
            // textMessage
            // 
            this.textMessage.AutoSize = true;
            this.textMessage.BackColor = System.Drawing.Color.Transparent;
            this.textMessage.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textMessage.Location = new System.Drawing.Point(3, 112);
            this.textMessage.Name = "textMessage";
            this.textMessage.Size = new System.Drawing.Size(84, 18);
            this.textMessage.TabIndex = 6;
            this.textMessage.Text = "Message:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(219, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(140, 69);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // SendMessageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(341, 269);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textMessage);
            this.Controls.Add(this.MessageTextBox);
            this.Controls.Add(this.txtTo);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnSendMessage);
            this.Controls.Add(this.ToBoxMessage);
            this.Controls.Add(this.WriteMessage);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SendMessageForm";
            this.Text = "Message";
            this.Load += new System.EventHandler(this.SendMessageForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void SendMessageForm_Load(object sender, System.EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void btnBack_Click(object sender, System.EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Label WriteMessage;
        private System.Windows.Forms.TextBox ToBoxMessage;
        private System.Windows.Forms.Button btnSendMessage;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label txtTo;
        private System.Windows.Forms.TextBox MessageTextBox;
        private System.Windows.Forms.Label textMessage;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class User : Resident // דייר פשוט
    {
        DB dataBase = DB.GetInstance();

        private int appartmentID;
        private string status; //(rent,purchased)
        private string religiousShabat;
        private string paymentBills;


        public User() { }

        public User(string FirstName, string LastName,
            int Id, int Telephone, string Email, string username, string password, string tag, int appartmentID,
            string status, string religiousShabat, string paymentBills, string bildingAdress) :
            base(FirstName, LastName, Id, Telephone, Email, username, password, tag = "User", bildingAdress)
        {
            this.appartmentID = appartmentID;
            this.status = status;
            this.religiousShabat = religiousShabat;
            this.paymentBills = paymentBills;


        }
        public int AppartmentID
        {
            get { return this.appartmentID; }
            set { this.appartmentID = value; }
        }
        public string Status
        {
            get { return this.status; }
            set { this.status = value; }
        }
        public string ReligiousShabat
        {
            get { return this.religiousShabat; }
            set { this.religiousShabat = value; }
        }
        public string PaymentBills
        {
            get { return this.paymentBills; }
            set { this.paymentBills = value; }
        }

        public List<Message> ViewMessage(int fromId)
        {
            return dataBase.ViewMessage(fromId);

        }

        public void makePayment(int sum)//pay the rent
        {
            int from = this.Id;
            this.PayStatus = true;
            dataBase.MakePayment(sum, from);
        }

        public void sendReport(string report)//send a report
        { int to=0;
            Message rep = new Message(report, this.Id,to);
            rep.IsReport=true;
            dataBase.SendReport(rep);         
        }

        public override void sendMessage(string text, int to)//send a message
        {
            Message mse = new Message(text, this.Id, to);
            dataBase.SendMessage(mse);
        }

        public void sendRequest(string request)//send a request
        {int to=0;
            Message req = new Message(request, this.Id, to);
            req.IsRequest=true;
            dataBase.SendRequest(req);
        }
             

        public void geTaxi(string destination)//calling to a taxi driver
        {
            Order texi = new Order(destination, this.BildingAdress + " apartmant no#" + this.AppartmentID, "texi", "texi service");
            dataBase.Order(texi);
        }


        public void orderFood(string foodtype ,string company)//order food
        {
            Order orderfood = new Order(foodtype, this.BildingAdress + " apartmant no#" + this.AppartmentID, company, "food service");
            dataBase.Order(orderfood);
        }

        public override string ToString()//print user details
        {
            return base.ToString() + " \napartment:" + this.appartmentID + "\nstatus" + this.status + "\nreligios?:" + this.religiousShabat + "\t\npayment bills" + this.PaymentBills +
                "\t\n************************************************\t\n \t\n  ";
        }
        

           
        
    }
}

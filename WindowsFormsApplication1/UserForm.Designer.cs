﻿namespace WindowsFormsApplication1
{
    partial class UserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserForm));
            this.HelloUser = new System.Windows.Forms.Label();
            this.btnMakePayment = new System.Windows.Forms.Button();
            this.btnSendReport = new System.Windows.Forms.Button();
            this.btnSendRequest = new System.Windows.Forms.Button();
            this.btnOrders = new System.Windows.Forms.Button();
            this.btnSendMessage = new System.Windows.Forms.Button();
            this.btnSend_SendMessageUser = new System.Windows.Forms.Button();
            this.txtMessage_SendMessageUser = new System.Windows.Forms.TextBox();
            this.txtTo_SendMessageUser = new System.Windows.Forms.TextBox();
            this.lblTo_SendMessageUser = new System.Windows.Forms.Label();
            this.btnBack_User = new System.Windows.Forms.Button();
            this.btnNext_RecieveMessageUser = new System.Windows.Forms.Button();
            this.txtMessage_RecieveMessage = new System.Windows.Forms.TextBox();
            this.btnRecieveMessage_user = new System.Windows.Forms.Button();
            this.btnSendReport_User = new System.Windows.Forms.Button();
            this.txtSendReport_User = new System.Windows.Forms.TextBox();
            this.lblWriteReport_User = new System.Windows.Forms.Label();
            this.btnSendRequest_User = new System.Windows.Forms.Button();
            this.txtSendRequest_User = new System.Windows.Forms.TextBox();
            this.lblTypeRequest_User = new System.Windows.Forms.Label();
            this.rBtnOrderFood_User = new System.Windows.Forms.RadioButton();
            this.rBtnOrderTaxi_User = new System.Windows.Forms.RadioButton();
            this.txtOrder_user = new System.Windows.Forms.TextBox();
            this.btnOrder_User = new System.Windows.Forms.Button();
            this.btnPay_User = new System.Windows.Forms.Button();
            this.txtSumToPay_User = new System.Windows.Forms.TextBox();
            this.lblSumToPay_User = new System.Windows.Forms.Label();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.labFoodCom = new System.Windows.Forms.Label();
            this.lblDestanition = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // HelloUser
            // 
            this.HelloUser.AutoSize = true;
            this.HelloUser.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HelloUser.Location = new System.Drawing.Point(12, 20);
            this.HelloUser.Name = "HelloUser";
            this.HelloUser.Size = new System.Drawing.Size(104, 22);
            this.HelloUser.TabIndex = 0;
            this.HelloUser.Text = "Hello User";
            // 
            // btnMakePayment
            // 
            this.btnMakePayment.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMakePayment.Location = new System.Drawing.Point(13, 212);
            this.btnMakePayment.Name = "btnMakePayment";
            this.btnMakePayment.Size = new System.Drawing.Size(127, 23);
            this.btnMakePayment.TabIndex = 1;
            this.btnMakePayment.Text = "Make payment";
            this.btnMakePayment.UseVisualStyleBackColor = true;
            this.btnMakePayment.Click += new System.EventHandler(this.MakePayment_Click);
            // 
            // btnSendReport
            // 
            this.btnSendReport.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSendReport.Location = new System.Drawing.Point(12, 125);
            this.btnSendReport.Name = "btnSendReport";
            this.btnSendReport.Size = new System.Drawing.Size(127, 23);
            this.btnSendReport.TabIndex = 2;
            this.btnSendReport.Text = "Send report";
            this.btnSendReport.UseVisualStyleBackColor = true;
            this.btnSendReport.Click += new System.EventHandler(this.btnSendReport_Click);
            // 
            // btnSendRequest
            // 
            this.btnSendRequest.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSendRequest.Location = new System.Drawing.Point(12, 154);
            this.btnSendRequest.Name = "btnSendRequest";
            this.btnSendRequest.Size = new System.Drawing.Size(127, 23);
            this.btnSendRequest.TabIndex = 3;
            this.btnSendRequest.Text = "Send request";
            this.btnSendRequest.UseVisualStyleBackColor = true;
            this.btnSendRequest.Click += new System.EventHandler(this.btnSendRequest_Click);
            // 
            // btnOrders
            // 
            this.btnOrders.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrders.Location = new System.Drawing.Point(12, 183);
            this.btnOrders.Name = "btnOrders";
            this.btnOrders.Size = new System.Drawing.Size(127, 23);
            this.btnOrders.TabIndex = 4;
            this.btnOrders.Text = "Orders";
            this.btnOrders.UseVisualStyleBackColor = true;
            this.btnOrders.Click += new System.EventHandler(this.btnOrders_Click);
            // 
            // btnSendMessage
            // 
            this.btnSendMessage.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSendMessage.Location = new System.Drawing.Point(12, 67);
            this.btnSendMessage.Name = "btnSendMessage";
            this.btnSendMessage.Size = new System.Drawing.Size(127, 23);
            this.btnSendMessage.TabIndex = 6;
            this.btnSendMessage.Text = "Send Message";
            this.btnSendMessage.UseVisualStyleBackColor = true;
            this.btnSendMessage.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // btnSend_SendMessageUser
            // 
            this.btnSend_SendMessageUser.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnSend_SendMessageUser.Location = new System.Drawing.Point(542, 214);
            this.btnSend_SendMessageUser.Name = "btnSend_SendMessageUser";
            this.btnSend_SendMessageUser.Size = new System.Drawing.Size(75, 23);
            this.btnSend_SendMessageUser.TabIndex = 10;
            this.btnSend_SendMessageUser.Text = "Send";
            this.btnSend_SendMessageUser.UseVisualStyleBackColor = true;
            this.btnSend_SendMessageUser.Visible = false;
            this.btnSend_SendMessageUser.Click += new System.EventHandler(this.btnSend_SendMessageUser_Click);
            // 
            // txtMessage_SendMessageUser
            // 
            this.txtMessage_SendMessageUser.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.txtMessage_SendMessageUser.Location = new System.Drawing.Point(146, 125);
            this.txtMessage_SendMessageUser.Multiline = true;
            this.txtMessage_SendMessageUser.Name = "txtMessage_SendMessageUser";
            this.txtMessage_SendMessageUser.Size = new System.Drawing.Size(391, 112);
            this.txtMessage_SendMessageUser.TabIndex = 11;
            this.txtMessage_SendMessageUser.Visible = false;
            // 
            // txtTo_SendMessageUser
            // 
            this.txtTo_SendMessageUser.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.txtTo_SendMessageUser.Location = new System.Drawing.Point(436, 69);
            this.txtTo_SendMessageUser.Name = "txtTo_SendMessageUser";
            this.txtTo_SendMessageUser.Size = new System.Drawing.Size(100, 23);
            this.txtTo_SendMessageUser.TabIndex = 12;
            this.txtTo_SendMessageUser.Visible = false;
            // 
            // lblTo_SendMessageUser
            // 
            this.lblTo_SendMessageUser.AutoSize = true;
            this.lblTo_SendMessageUser.BackColor = System.Drawing.Color.Transparent;
            this.lblTo_SendMessageUser.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.lblTo_SendMessageUser.Location = new System.Drawing.Point(394, 75);
            this.lblTo_SendMessageUser.Name = "lblTo_SendMessageUser";
            this.lblTo_SendMessageUser.Size = new System.Drawing.Size(27, 15);
            this.lblTo_SendMessageUser.TabIndex = 13;
            this.lblTo_SendMessageUser.Text = "To:";
            this.lblTo_SendMessageUser.Visible = false;
            // 
            // btnBack_User
            // 
            this.btnBack_User.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnBack_User.Location = new System.Drawing.Point(13, 256);
            this.btnBack_User.Name = "btnBack_User";
            this.btnBack_User.Size = new System.Drawing.Size(82, 23);
            this.btnBack_User.TabIndex = 14;
            this.btnBack_User.Text = "Back";
            this.btnBack_User.UseVisualStyleBackColor = true;
            this.btnBack_User.Click += new System.EventHandler(this.btnBack_User_Click);
            // 
            // btnNext_RecieveMessageUser
            // 
            this.btnNext_RecieveMessageUser.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnNext_RecieveMessageUser.Location = new System.Drawing.Point(542, 211);
            this.btnNext_RecieveMessageUser.Name = "btnNext_RecieveMessageUser";
            this.btnNext_RecieveMessageUser.Size = new System.Drawing.Size(75, 23);
            this.btnNext_RecieveMessageUser.TabIndex = 15;
            this.btnNext_RecieveMessageUser.Text = "Next";
            this.btnNext_RecieveMessageUser.UseVisualStyleBackColor = true;
            this.btnNext_RecieveMessageUser.Visible = false;
            this.btnNext_RecieveMessageUser.Click += new System.EventHandler(this.btnNext_RecieveMessageUser_Click);
            // 
            // txtMessage_RecieveMessage
            // 
            this.txtMessage_RecieveMessage.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.txtMessage_RecieveMessage.Location = new System.Drawing.Point(145, 93);
            this.txtMessage_RecieveMessage.Multiline = true;
            this.txtMessage_RecieveMessage.Name = "txtMessage_RecieveMessage";
            this.txtMessage_RecieveMessage.ReadOnly = true;
            this.txtMessage_RecieveMessage.Size = new System.Drawing.Size(390, 170);
            this.txtMessage_RecieveMessage.TabIndex = 16;
            this.txtMessage_RecieveMessage.Text = "To See Your Messages Press The \"Next\" Button";
            this.txtMessage_RecieveMessage.Visible = false;
            // 
            // btnRecieveMessage_user
            // 
            this.btnRecieveMessage_user.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnRecieveMessage_user.Location = new System.Drawing.Point(12, 97);
            this.btnRecieveMessage_user.Name = "btnRecieveMessage_user";
            this.btnRecieveMessage_user.Size = new System.Drawing.Size(127, 23);
            this.btnRecieveMessage_user.TabIndex = 17;
            this.btnRecieveMessage_user.Text = "Recieve Message";
            this.btnRecieveMessage_user.UseVisualStyleBackColor = true;
            this.btnRecieveMessage_user.Click += new System.EventHandler(this.btnRecieveMessage_user_Click);
            // 
            // btnSendReport_User
            // 
            this.btnSendReport_User.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnSendReport_User.Location = new System.Drawing.Point(542, 212);
            this.btnSendReport_User.Name = "btnSendReport_User";
            this.btnSendReport_User.Size = new System.Drawing.Size(75, 23);
            this.btnSendReport_User.TabIndex = 18;
            this.btnSendReport_User.Text = "Send";
            this.btnSendReport_User.UseVisualStyleBackColor = true;
            this.btnSendReport_User.Visible = false;
            this.btnSendReport_User.Click += new System.EventHandler(this.btnSendReport_User_Click);
            // 
            // txtSendReport_User
            // 
            this.txtSendReport_User.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.txtSendReport_User.Location = new System.Drawing.Point(146, 97);
            this.txtSendReport_User.Multiline = true;
            this.txtSendReport_User.Name = "txtSendReport_User";
            this.txtSendReport_User.Size = new System.Drawing.Size(390, 140);
            this.txtSendReport_User.TabIndex = 19;
            this.txtSendReport_User.Visible = false;
            // 
            // lblWriteReport_User
            // 
            this.lblWriteReport_User.AutoSize = true;
            this.lblWriteReport_User.BackColor = System.Drawing.Color.Transparent;
            this.lblWriteReport_User.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.lblWriteReport_User.Location = new System.Drawing.Point(146, 67);
            this.lblWriteReport_User.Name = "lblWriteReport_User";
            this.lblWriteReport_User.Size = new System.Drawing.Size(126, 15);
            this.lblWriteReport_User.TabIndex = 20;
            this.lblWriteReport_User.Text = "Write Your Report:";
            this.lblWriteReport_User.Visible = false;
            // 
            // btnSendRequest_User
            // 
            this.btnSendRequest_User.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnSendRequest_User.Location = new System.Drawing.Point(542, 211);
            this.btnSendRequest_User.Name = "btnSendRequest_User";
            this.btnSendRequest_User.Size = new System.Drawing.Size(75, 23);
            this.btnSendRequest_User.TabIndex = 21;
            this.btnSendRequest_User.Text = "Send";
            this.btnSendRequest_User.UseVisualStyleBackColor = true;
            this.btnSendRequest_User.Visible = false;
            this.btnSendRequest_User.Click += new System.EventHandler(this.btnSendRequest_User_Click);
            // 
            // txtSendRequest_User
            // 
            this.txtSendRequest_User.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.txtSendRequest_User.Location = new System.Drawing.Point(146, 97);
            this.txtSendRequest_User.Multiline = true;
            this.txtSendRequest_User.Name = "txtSendRequest_User";
            this.txtSendRequest_User.Size = new System.Drawing.Size(390, 140);
            this.txtSendRequest_User.TabIndex = 22;
            this.txtSendRequest_User.Visible = false;
            // 
            // lblTypeRequest_User
            // 
            this.lblTypeRequest_User.AutoSize = true;
            this.lblTypeRequest_User.BackColor = System.Drawing.Color.Transparent;
            this.lblTypeRequest_User.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.lblTypeRequest_User.Location = new System.Drawing.Point(146, 67);
            this.lblTypeRequest_User.Name = "lblTypeRequest_User";
            this.lblTypeRequest_User.Size = new System.Drawing.Size(131, 15);
            this.lblTypeRequest_User.TabIndex = 23;
            this.lblTypeRequest_User.Text = "Type Your Request:";
            this.lblTypeRequest_User.Visible = false;
            // 
            // rBtnOrderFood_User
            // 
            this.rBtnOrderFood_User.AutoSize = true;
            this.rBtnOrderFood_User.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.rBtnOrderFood_User.Location = new System.Drawing.Point(525, 93);
            this.rBtnOrderFood_User.Name = "rBtnOrderFood_User";
            this.rBtnOrderFood_User.Size = new System.Drawing.Size(98, 19);
            this.rBtnOrderFood_User.TabIndex = 24;
            this.rBtnOrderFood_User.TabStop = true;
            this.rBtnOrderFood_User.Text = "Order Food";
            this.rBtnOrderFood_User.UseVisualStyleBackColor = true;
            this.rBtnOrderFood_User.Visible = false;
            this.rBtnOrderFood_User.CheckedChanged += new System.EventHandler(this.rBtnOrderFood_User_CheckedChanged);
            // 
            // rBtnOrderTaxi_User
            // 
            this.rBtnOrderTaxi_User.AutoSize = true;
            this.rBtnOrderTaxi_User.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.rBtnOrderTaxi_User.Location = new System.Drawing.Point(525, 125);
            this.rBtnOrderTaxi_User.Name = "rBtnOrderTaxi_User";
            this.rBtnOrderTaxi_User.Size = new System.Drawing.Size(93, 19);
            this.rBtnOrderTaxi_User.TabIndex = 25;
            this.rBtnOrderTaxi_User.TabStop = true;
            this.rBtnOrderTaxi_User.Text = "Order Taxi";
            this.rBtnOrderTaxi_User.UseVisualStyleBackColor = true;
            this.rBtnOrderTaxi_User.Visible = false;
            this.rBtnOrderTaxi_User.CheckedChanged += new System.EventHandler(this.rBtnOrderTaxi_User_CheckedChanged);
            // 
            // txtOrder_user
            // 
            this.txtOrder_user.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.txtOrder_user.Location = new System.Drawing.Point(146, 90);
            this.txtOrder_user.Multiline = true;
            this.txtOrder_user.Name = "txtOrder_user";
            this.txtOrder_user.Size = new System.Drawing.Size(337, 131);
            this.txtOrder_user.TabIndex = 26;
            this.txtOrder_user.Visible = false;
            // 
            // btnOrder_User
            // 
            this.btnOrder_User.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnOrder_User.Location = new System.Drawing.Point(543, 212);
            this.btnOrder_User.Name = "btnOrder_User";
            this.btnOrder_User.Size = new System.Drawing.Size(75, 22);
            this.btnOrder_User.TabIndex = 27;
            this.btnOrder_User.Text = "Order";
            this.btnOrder_User.UseVisualStyleBackColor = true;
            this.btnOrder_User.Visible = false;
            this.btnOrder_User.Click += new System.EventHandler(this.btnOrder_User_Click);
            // 
            // btnPay_User
            // 
            this.btnPay_User.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.btnPay_User.Location = new System.Drawing.Point(542, 99);
            this.btnPay_User.Name = "btnPay_User";
            this.btnPay_User.Size = new System.Drawing.Size(75, 23);
            this.btnPay_User.TabIndex = 28;
            this.btnPay_User.Text = "Pay";
            this.btnPay_User.UseVisualStyleBackColor = true;
            this.btnPay_User.Visible = false;
            this.btnPay_User.Click += new System.EventHandler(this.btnPay_User_Click);
            // 
            // txtSumToPay_User
            // 
            this.txtSumToPay_User.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtSumToPay_User.Location = new System.Drawing.Point(145, 93);
            this.txtSumToPay_User.Name = "txtSumToPay_User";
            this.txtSumToPay_User.Size = new System.Drawing.Size(387, 20);
            this.txtSumToPay_User.TabIndex = 29;
            this.txtSumToPay_User.Visible = false;
            // 
            // lblSumToPay_User
            // 
            this.lblSumToPay_User.AutoSize = true;
            this.lblSumToPay_User.BackColor = System.Drawing.Color.Transparent;
            this.lblSumToPay_User.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblSumToPay_User.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F);
            this.lblSumToPay_User.Location = new System.Drawing.Point(146, 72);
            this.lblSumToPay_User.Name = "lblSumToPay_User";
            this.lblSumToPay_User.Size = new System.Drawing.Size(172, 15);
            this.lblSumToPay_User.TabIndex = 30;
            this.lblSumToPay_User.Text = "Please Enter Sum To Pay:";
            this.lblSumToPay_User.Visible = false;
            // 
            // txtCompany
            // 
            this.txtCompany.Location = new System.Drawing.Point(211, 128);
            this.txtCompany.Margin = new System.Windows.Forms.Padding(2);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(222, 20);
            this.txtCompany.TabIndex = 31;
            this.txtCompany.Visible = false;
            // 
            // labFoodCom
            // 
            this.labFoodCom.AutoSize = true;
            this.labFoodCom.BackColor = System.Drawing.Color.Transparent;
            this.labFoodCom.Location = new System.Drawing.Point(257, 100);
            this.labFoodCom.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labFoodCom.Name = "labFoodCom";
            this.labFoodCom.Size = new System.Drawing.Size(134, 13);
            this.labFoodCom.TabIndex = 32;
            this.labFoodCom.Text = "Enter food company name.";
            this.labFoodCom.Visible = false;
            // 
            // lblDestanition
            // 
            this.lblDestanition.AutoSize = true;
            this.lblDestanition.BackColor = System.Drawing.Color.Transparent;
            this.lblDestanition.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblDestanition.Location = new System.Drawing.Point(147, 67);
            this.lblDestanition.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDestanition.Name = "lblDestanition";
            this.lblDestanition.Size = new System.Drawing.Size(152, 24);
            this.lblDestanition.TabIndex = 33;
            this.lblDestanition.Text = "Enter Destination";
            this.lblDestanition.Visible = false;
            // 
            // UserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(649, 357);
            this.Controls.Add(this.lblDestanition);
            this.Controls.Add(this.labFoodCom);
            this.Controls.Add(this.txtCompany);
            this.Controls.Add(this.lblSumToPay_User);
            this.Controls.Add(this.txtSumToPay_User);
            this.Controls.Add(this.btnPay_User);
            this.Controls.Add(this.btnOrder_User);
            this.Controls.Add(this.txtOrder_user);
            this.Controls.Add(this.rBtnOrderTaxi_User);
            this.Controls.Add(this.rBtnOrderFood_User);
            this.Controls.Add(this.lblTypeRequest_User);
            this.Controls.Add(this.txtSendRequest_User);
            this.Controls.Add(this.btnSendRequest_User);
            this.Controls.Add(this.lblWriteReport_User);
            this.Controls.Add(this.txtSendReport_User);
            this.Controls.Add(this.btnSendReport_User);
            this.Controls.Add(this.btnRecieveMessage_user);
            this.Controls.Add(this.txtMessage_RecieveMessage);
            this.Controls.Add(this.btnNext_RecieveMessageUser);
            this.Controls.Add(this.btnBack_User);
            this.Controls.Add(this.lblTo_SendMessageUser);
            this.Controls.Add(this.txtTo_SendMessageUser);
            this.Controls.Add(this.txtMessage_SendMessageUser);
            this.Controls.Add(this.btnSend_SendMessageUser);
            this.Controls.Add(this.btnSendMessage);
            this.Controls.Add(this.btnOrders);
            this.Controls.Add(this.btnSendRequest);
            this.Controls.Add(this.btnSendReport);
            this.Controls.Add(this.btnMakePayment);
            this.Controls.Add(this.HelloUser);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserForm";
            this.Text = "User";
            this.Load += new System.EventHandler(this.UserForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void btnBack_Click(object sender, System.EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        

        

        #endregion

        private System.Windows.Forms.Label HelloUser;
        private System.Windows.Forms.Button btnMakePayment;
        private System.Windows.Forms.Button btnSendReport;
        private System.Windows.Forms.Button btnSendRequest;
        private System.Windows.Forms.Button btnOrders;
        private System.Windows.Forms.Button btnSendMessage;
        private System.Windows.Forms.Button btnSend_SendMessageUser;
        private System.Windows.Forms.TextBox txtMessage_SendMessageUser;
        private System.Windows.Forms.TextBox txtTo_SendMessageUser;
        private System.Windows.Forms.Label lblTo_SendMessageUser;
        private System.Windows.Forms.Button btnBack_User;
        private System.Windows.Forms.Button btnNext_RecieveMessageUser;
        private System.Windows.Forms.TextBox txtMessage_RecieveMessage;
        private System.Windows.Forms.Button btnRecieveMessage_user;
        private System.Windows.Forms.Button btnSendReport_User;
        private System.Windows.Forms.TextBox txtSendReport_User;
        private System.Windows.Forms.Label lblWriteReport_User;
        private System.Windows.Forms.Button btnSendRequest_User;
        private System.Windows.Forms.TextBox txtSendRequest_User;
        private System.Windows.Forms.Label lblTypeRequest_User;
        private System.Windows.Forms.RadioButton rBtnOrderFood_User;
        private System.Windows.Forms.RadioButton rBtnOrderTaxi_User;
        private System.Windows.Forms.TextBox txtOrder_user;
        private System.Windows.Forms.Button btnOrder_User;
        private System.Windows.Forms.Button btnPay_User;
        private System.Windows.Forms.TextBox txtSumToPay_User;
        private System.Windows.Forms.Label lblSumToPay_User;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.Label labFoodCom;
        private System.Windows.Forms.Label lblDestanition;
    }
}
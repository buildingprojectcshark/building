﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{

    partial class UserForm : Form
    {

        User user;
        static int index = 0;
        
        List<Message> messages;
        
        
        public UserForm()
        {
            InitializeComponent();
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
        }
        
        public UserForm(User user)
        {
            this.user = user;

            InitializeComponent();

        }
        /************************************Welcome Text *************************************************************************/
        private void UserForm_Load(object sender, EventArgs e)
        {
            HelloUser.Text = "Welcome " + user.LastName.ToUpper() + " " + user.FirstName.ToUpper();
        }
        // ***********************************Added closing on X click******************************************************************/
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;

            // Confirm user wants to close
            switch (MessageBox.Show(this, "Are you sure you want to close?", "Closing", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    e.Cancel = true;
                    break;
                default:
                    break;
            }
        }
        // ***********************************Back Button(Opens New Login Form******************************************************************/
        private void btnBack_User_Click(object sender, EventArgs e)
        {
            LoginForm back = new LoginForm();
            this.Hide();
            back.Show();
            this.Owner = back;
        }
        /************************************MEssage Buttons And Methods(Send and Recieve)******************************************************/
        private void btnSendMessage_Click(object sender, EventArgs e)
        {
            txtSendReport_User.Visible = false;
            btnSendReport_User.Visible = false;
            txtMessage_RecieveMessage.Visible = false;
            btnNext_RecieveMessageUser.Visible = false;
            lblWriteReport_User.Visible = false;
            lblTypeRequest_User.Visible = false;
            txtSendRequest_User.Visible = false;
            btnSendRequest_User.Visible = false;
            txtOrder_user.Visible = false;
            rBtnOrderFood_User.Visible = false;
            rBtnOrderTaxi_User.Visible = false;
            btnOrder_User.Visible = false;
            txtSumToPay_User.Visible = false;
            lblSumToPay_User.Visible = false;
            btnPay_User.Visible = false;
            rBtnOrderFood_User.Visible = false;
            rBtnOrderTaxi_User.Visible = false;
            txtCompany.Visible = false;
            labFoodCom.Visible = false;
            btnOrder_User.Visible = false;
            lblDestanition.Visible = false;
            btnOrder_User.Visible = false;
            txtOrder_user.Visible = false;

            lblTo_SendMessageUser.Visible = true;
            txtMessage_SendMessageUser.Visible = true;
            txtTo_SendMessageUser.Visible = true;
            btnSend_SendMessageUser.Visible = true;
        }

        private void btnRecieveMessage_user_Click(object sender, EventArgs e)
        {
            txtSendReport_User.Visible = false;
            btnSendReport_User.Visible = false;
            lblTo_SendMessageUser.Visible = false;
            txtMessage_SendMessageUser.Visible = false;
            txtTo_SendMessageUser.Visible = false;
            btnSend_SendMessageUser.Visible = false;
            lblWriteReport_User.Visible = false;
            lblTypeRequest_User.Visible = false;
            txtSendRequest_User.Visible = false;
            btnSendRequest_User.Visible = false;
            txtOrder_user.Visible = false;
            rBtnOrderFood_User.Visible = false;
            rBtnOrderTaxi_User.Visible = false;
            btnOrder_User.Visible = false;
            txtSumToPay_User.Visible = false;
            lblSumToPay_User.Visible = false;
            btnPay_User.Visible = false;
            rBtnOrderFood_User.Visible = false;
            rBtnOrderTaxi_User.Visible = false;
            txtCompany.Visible = false;
            labFoodCom.Visible = false;
            btnOrder_User.Visible = false;
            lblDestanition.Visible = false;
            btnOrder_User.Visible = false;
            txtOrder_user.Visible = false;

            txtMessage_RecieveMessage.Visible = true;
            btnNext_RecieveMessageUser.Visible = true;
            messages = user.ViewMessage(user.Id);
            index = 0;
        }

        private void btnSend_SendMessageUser_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(txtTo_SendMessageUser.Text);
            string text = txtMessage_SendMessageUser.Text;
            user.sendMessage(text, id);

            txtSendReport_User.Visible = false;
            btnSendReport_User.Visible = false;
            lblTo_SendMessageUser.Visible = false;
            txtMessage_SendMessageUser.Visible = false;
            txtTo_SendMessageUser.Visible = false;
            btnSend_SendMessageUser.Visible = false;
            lblWriteReport_User.Visible = false;
            txtMessage_RecieveMessage.Visible = false;
            btnNext_RecieveMessageUser.Visible = false;
            txtOrder_user.Visible = false;
            rBtnOrderFood_User.Visible = false;
            rBtnOrderTaxi_User.Visible = false;
            btnOrder_User.Visible = false;
            txtSumToPay_User.Visible = false;
            lblSumToPay_User.Visible = false;
            btnPay_User.Visible = false;
            rBtnOrderFood_User.Visible = false;
            rBtnOrderTaxi_User.Visible = false;
            txtCompany.Visible = false;
            labFoodCom.Visible = false;
            btnOrder_User.Visible = false;
            lblDestanition.Visible = false;
            btnOrder_User.Visible = false;
            txtOrder_user.Visible = false;

            lblTypeRequest_User.Visible = false;
            txtSendRequest_User.Visible = false;
            btnSendRequest_User.Visible = false;
        }

        private void btnNext_RecieveMessageUser_Click(object sender, EventArgs e) //not working
        {
            while (index < messages.Count)
            {
                txtMessage_RecieveMessage.Text += messages[index++].ToString();
            }
        }


        /************************************Payments Buttons And Methods******************************************************/
        private void MakePayment_Click(object sender, EventArgs e)
        {
            txtSendReport_User.Visible = false;
            btnSendReport_User.Visible = false;
            txtMessage_RecieveMessage.Visible = false;
            btnNext_RecieveMessageUser.Visible = false;
            lblWriteReport_User.Visible = false;
            lblTypeRequest_User.Visible = false;
            txtSendRequest_User.Visible = false;
            btnSendRequest_User.Visible = false;
            lblTo_SendMessageUser.Visible = false;
            txtMessage_SendMessageUser.Visible = false;
            txtTo_SendMessageUser.Visible = false;
            btnSend_SendMessageUser.Visible = false;
            txtOrder_user.Visible = false;
            rBtnOrderFood_User.Visible = false;
            rBtnOrderTaxi_User.Visible = false;
            btnOrder_User.Visible = false;
            rBtnOrderFood_User.Visible = false;
            rBtnOrderTaxi_User.Visible = false;
            txtCompany.Visible = false;
            labFoodCom.Visible = false;
            btnOrder_User.Visible = false;
            lblDestanition.Visible = false;
            btnOrder_User.Visible = false;
            txtOrder_user.Visible = false;


            txtSumToPay_User.Visible = true;
            lblSumToPay_User.Visible = true;
            btnPay_User.Visible = true;
        }

        private void btnPay_User_Click(object sender, EventArgs e)//not working
        {
            int sum = Int32.Parse(txtSumToPay_User.Text);
            if(sum<0)
            {
                MessageBox.Show("Who do you want to fool, ha?");
            }
            else user.makePayment(sum);
        }


        /************************************Orders Buttons And Methods(Send and Recieve)******************************************************/
        private void btnOrders_Click(object sender, EventArgs e)
        {
            txtSendReport_User.Visible = false;
            btnSendReport_User.Visible = false;
            txtMessage_RecieveMessage.Visible = false;
            btnNext_RecieveMessageUser.Visible = false;
            lblWriteReport_User.Visible = false;
            lblTypeRequest_User.Visible = false;
            txtSendRequest_User.Visible = false;
            btnSendRequest_User.Visible = false;
            lblTo_SendMessageUser.Visible = false;
            txtMessage_SendMessageUser.Visible = false;
            txtTo_SendMessageUser.Visible = false;
            btnSend_SendMessageUser.Visible = false;
            txtSumToPay_User.Visible = false;
            lblSumToPay_User.Visible = false;
            btnPay_User.Visible = false;


            txtOrder_user.Visible = true;
            rBtnOrderFood_User.Visible = true;
            rBtnOrderTaxi_User.Visible = true;
            btnOrder_User.Visible = true;

        }
           
            /*************Nvigate between  food and taxi order*******************/
           
            private void rBtnOrderFood_User_CheckedChanged(object sender, EventArgs e)
        {
            if (rBtnOrderFood_User.Checked==true)
            {
                txtSendReport_User.Visible = false;
                btnSendReport_User.Visible = false;
                txtMessage_RecieveMessage.Visible = false;
                btnNext_RecieveMessageUser.Visible = false;
                lblWriteReport_User.Visible = false;
                lblTypeRequest_User.Visible = false;
                txtSendRequest_User.Visible = false;
                btnSendRequest_User.Visible = false;
                lblTo_SendMessageUser.Visible = false;
                txtMessage_SendMessageUser.Visible = false;
                txtTo_SendMessageUser.Visible = false;
                btnSend_SendMessageUser.Visible = false;
                txtSumToPay_User.Visible = false;
                lblSumToPay_User.Visible = false;
                btnPay_User.Visible = false;
                txtOrder_user.Visible = false;
                lblDestanition.Visible = false;
                txtOrder_user.Visible = false;

                rBtnOrderFood_User.Visible = true;
                rBtnOrderTaxi_User.Visible = true;
                txtCompany.Visible = true;
                labFoodCom.Visible = true;
                btnOrder_User.Visible = true;

                
            }
            if (rBtnOrderTaxi_User.Checked)
            {
                txtSendReport_User.Visible = false;
                btnSendReport_User.Visible = false;
                txtMessage_RecieveMessage.Visible = false;
                btnNext_RecieveMessageUser.Visible = false;
                lblWriteReport_User.Visible = false;
                lblTypeRequest_User.Visible = false;
                txtSendRequest_User.Visible = false;
                btnSendRequest_User.Visible = false;
                lblTo_SendMessageUser.Visible = false;
                txtMessage_SendMessageUser.Visible = false;
                txtTo_SendMessageUser.Visible = false;
                btnSend_SendMessageUser.Visible = false;
                txtSumToPay_User.Visible = false;
                lblSumToPay_User.Visible = false;
                btnPay_User.Visible = false;
                txtOrder_user.Visible = false;
                rBtnOrderFood_User.Visible = false;
                rBtnOrderTaxi_User.Visible = false;
                txtCompany.Visible = false;
                labFoodCom.Visible = false;

                lblDestanition.Visible = true;
                btnOrder_User.Visible = true;
                txtOrder_user.Visible = true;
            }
        }
        
        private void btnOrder_User_Click(object sender, EventArgs e)
        {
           string decription;
           decription=txtOrder_user.Text;
            if(rBtnOrderFood_User.Checked)
            {
                txtCompany.Visible = true;
                labFoodCom.Visible = true;
                lblDestanition.Visible = false;
                txtSendReport_User.Visible = false;
                btnSendReport_User.Visible = false;
                txtMessage_RecieveMessage.Visible = false;
                btnNext_RecieveMessageUser.Visible = false;
                lblWriteReport_User.Visible = false;
                lblTypeRequest_User.Visible = false;
                txtSendRequest_User.Visible = false;
                btnSendRequest_User.Visible = false;
                lblTo_SendMessageUser.Visible = false;
                txtMessage_SendMessageUser.Visible = false;
                txtTo_SendMessageUser.Visible = false;
                btnSend_SendMessageUser.Visible = false;
                txtSumToPay_User.Visible = false;
                lblSumToPay_User.Visible = false;
                btnPay_User.Visible = false;
                txtOrder_user.Visible = false;
                rBtnOrderFood_User.Visible = true;
                rBtnOrderTaxi_User.Visible = true;
                btnOrder_User.Visible = false;
                
                
                
                string company = txtCompany.Text;
                user.orderFood(decription,company);
                txtCompany.Clear();
            }
            if(rBtnOrderTaxi_User.Checked)
            {
                lblDestanition.Visible = true;
                user.geTaxi(decription);

            }
            txtCompany.Visible = false;
            labFoodCom.Visible = false;
            lblDestanition.Visible = false;
            txtOrder_user.Clear();
        }

        private void rBtnOrderTaxi_User_CheckedChanged(object sender, EventArgs e)
        {
            txtCompany.Visible = false;
            labFoodCom.Visible = false;
            lblDestanition.Visible = true;
        }

/************************************Report Buttons And Methods(Send and Recieve)******************************************************/
        private void btnSendReport_Click(object sender, System.EventArgs e)
        {
            txtMessage_RecieveMessage.Visible = false;
            btnNext_RecieveMessageUser.Visible = false;
            lblTo_SendMessageUser.Visible = false;
            txtMessage_SendMessageUser.Visible = false;
            txtTo_SendMessageUser.Visible = false;
            btnSend_SendMessageUser.Visible = false;
            lblTypeRequest_User.Visible = false;
            txtSendRequest_User.Visible = false;
            btnSendRequest_User.Visible = false;
            txtOrder_user.Visible = false;
            rBtnOrderFood_User.Visible = false;
            rBtnOrderTaxi_User.Visible = false;
            btnOrder_User.Visible = false;
            txtSumToPay_User.Visible = false;
            lblSumToPay_User.Visible = false;
            btnPay_User.Visible = false;
            rBtnOrderFood_User.Visible = false;
            rBtnOrderTaxi_User.Visible = false;
            txtCompany.Visible = false;
            labFoodCom.Visible = false;
            btnOrder_User.Visible = false;
            lblDestanition.Visible = false;
            btnOrder_User.Visible = false;
            txtOrder_user.Visible = false;

            txtSendReport_User.Visible = true;
            btnSendReport_User.Visible = true;
            lblWriteReport_User.Visible = true;
        }

        private void btnSendReport_User_Click(object sender, EventArgs e)
        {
            string report=txtSendReport_User.Text;
            user.sendReport(report);
            txtSendReport_User.Clear();
        }

        

        private void btnSendRequest_Click(object sender, EventArgs e)
        {
            txtSendReport_User.Visible = false;
            btnSendReport_User.Visible = false;
            lblTo_SendMessageUser.Visible = false;
            txtMessage_SendMessageUser.Visible = false;
            txtTo_SendMessageUser.Visible = false;
            btnSend_SendMessageUser.Visible = false;
            lblWriteReport_User.Visible = false;
            txtMessage_RecieveMessage.Visible = false;
            btnNext_RecieveMessageUser.Visible = false;
            txtOrder_user.Visible = false;
            rBtnOrderFood_User.Visible = false;
            rBtnOrderTaxi_User.Visible = false;
            btnOrder_User.Visible = false;
            txtSumToPay_User.Visible = false;
            lblSumToPay_User.Visible = false;
            btnPay_User.Visible = false;
            rBtnOrderFood_User.Visible = false;
            rBtnOrderTaxi_User.Visible = false;
            txtCompany.Visible = false;
            labFoodCom.Visible = false;
            btnOrder_User.Visible = false;
            lblDestanition.Visible = false;
            btnOrder_User.Visible = false;
            txtOrder_user.Visible = false;

            lblTypeRequest_User.Visible = true;
            txtSendRequest_User.Visible = true;
            btnSendRequest_User.Visible = true;
        }

        private void btnSendRequest_User_Click(object sender, EventArgs e)
        {
            string reqest = txtSendRequest_User.Text;
            user.sendRequest(reqest);
            txtSendRequest_User.Clear();
        }
       
    }
}

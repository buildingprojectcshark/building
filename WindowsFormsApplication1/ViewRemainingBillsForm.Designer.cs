﻿namespace WindowsFormsApplication1
{
    partial class ViewRemainingBillsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewRemainingBillsForm));
            this.ViewRemainingBills = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Back = new System.Windows.Forms.Button();
            this.RemainingBillsBox = new System.Windows.Forms.RichTextBox();
            this.View = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ViewRemainingBills
            // 
            this.ViewRemainingBills.AutoSize = true;
            this.ViewRemainingBills.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewRemainingBills.Location = new System.Drawing.Point(12, 9);
            this.ViewRemainingBills.Name = "ViewRemainingBills";
            this.ViewRemainingBills.Size = new System.Drawing.Size(192, 22);
            this.ViewRemainingBills.TabIndex = 0;
            this.ViewRemainingBills.Text = "View remaining bills";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(210, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(134, 125);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // Back
            // 
            this.Back.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Back.Location = new System.Drawing.Point(254, 234);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(75, 23);
            this.Back.TabIndex = 2;
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = true;
            // 
            // RemainingBillsBox
            // 
            this.RemainingBillsBox.BackColor = System.Drawing.Color.LightBlue;
            this.RemainingBillsBox.Location = new System.Drawing.Point(16, 34);
            this.RemainingBillsBox.Name = "RemainingBillsBox";
            this.RemainingBillsBox.Size = new System.Drawing.Size(188, 172);
            this.RemainingBillsBox.TabIndex = 4;
            this.RemainingBillsBox.Text = "";
            // 
            // View
            // 
            this.View.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.View.Location = new System.Drawing.Point(129, 212);
            this.View.Name = "View";
            this.View.Size = new System.Drawing.Size(75, 23);
            this.View.TabIndex = 5;
            this.View.Text = "View";
            this.View.UseVisualStyleBackColor = true;
            // 
            // ViewRemainingBillsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(341, 269);
            this.Controls.Add(this.View);
            this.Controls.Add(this.RemainingBillsBox);
            this.Controls.Add(this.Back);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.ViewRemainingBills);
            this.DoubleBuffered = true;
            this.Name = "ViewRemainingBillsForm";
            this.Text = "View Remaining Bills";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ViewRemainingBills;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.RichTextBox RemainingBillsBox;
        private System.Windows.Forms.Button View;
    }
}